@JsonCadenceConversion(NFTViewJsonCadenceConverter::class)
data class NFTView(
    val id: ULong,
    val archetype: ArchetypeView,
    val artifact: ArtifactView,
    val print: PrintView?,
    val faucet: FaucetView?,
    val set: SetView?,
    val serialNumber: ULong,
    val metadata: Map<String, MetadataField>
)

class NFTViewJsonCadenceConverter : JsonCadenceConverter<NFTView> {
    override fun marshall(value: NFTView, namespace: CadenceNamespace): Field<*> = marshall {
        struct {
            compositeOfNamedMap(namespace.withNamespace("NFTView")) {
                mapOf(
                    "id" to uint64(value.id.toString()),
                    "archetype" to marshall(value.archetype, ArchetypeView::class, namespace),
                    "artifact" to marshall(value.artifact, ArtifactView::class, namespace),
                    "print" to optional(value.print?.let { p -> marshall(p, PrintView::class, namespace) }),
                    "faucet" to optional(value.faucet?.let { p -> marshall(p, FaucetView::class, namespace) }),
                    "set" to optional(value.set?.let { p -> marshall(p, SetView::class, namespace) }),
                    "serialNumber" to uint64(value.serialNumber),
                    "metadata" to dictionaryOfNamed(value.metadata) { it.key to marshall(it.value, MetadataField::class, namespace) }
                )
            }
        }
    }

    override fun unmarshall(value: Field<*>, namespace: CadenceNamespace): NFTView = unmarshall(value) {
        NFTView(
            id = ulong("id"),
            archetype = unmarshall("archetype", namespace),
            artifact = unmarshall("artifact", namespace),
            print = optional("print") { unmarshall(it, namespace) },
            faucet = optional("faucet") { unmarshall(it, namespace) },
            set = optional("set") { unmarshall(it, namespace) },
            serialNumber = ulong("serialNumber"),
            metadata = dictionaryMap("metadata") { k, v -> string(k) to unmarshall(v, MetadataField::class, namespace) }
        )
    }
}