    override fun getNFTView(address: FlowAddress, id: ULong): NFTView? {
        val result = flow.simpleFlowScript {
            script {
                """
                    import TenantService from ${address.formatted}
                    pub fun main(id: UInt64, ownerAddress: Address): TenantService.NFTView? {
                    
                        let ownerAccount = getAccount(ownerAddress)
                            
                        let publicCollection = ownerAccount.getCapability(/public/TenantNFTCollection)
                            .borrow<&{TenantService.CollectionPublic}>()
                            ?? panic("Could not find nft collection for that account")
                            
                        let nft = publicCollection.borrowNFTData(id: id)
                            ?? panic("Could not find NFT")
                        
                        return TenantService.getNFTView(nft)
                    }
                """.trimIndent()
            }
            arg { uint64(id) }
            arg { address(address.formatted) }
        }
        return (result.jsonCadence as OptionalField).value?.let { Flow.unmarshall(NFTView::class, it) }
    }