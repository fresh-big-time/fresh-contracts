    pub struct NFTView {
        pub let id: UInt64
        pub let archetype: ArchetypeView
        pub let artifact: ArtifactView
        pub let print: PrintView?
        pub let faucet: FaucetView?
        pub let set: SetView?
        pub let serialNumber: UInt64
        pub let metadata: {String: TenantService.MetadataField}

        init(
            id: UInt64,
            archetype: ArchetypeView,
            artifact: ArtifactView,
            print: PrintView?,
            faucet: FaucetView?,
            set: SetView?,
            serialNumber: UInt64,
            metadata: {String: TenantService.MetadataField}
        ) {
            self.id = id
            self.archetype = archetype
            self.artifact = artifact
            self.print = print
            self.faucet = faucet
            self.set = set
            self.serialNumber = serialNumber
            self.metadata = metadata
        }
    }