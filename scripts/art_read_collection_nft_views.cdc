import FreshArtNFT from "../contracts/FreshArtNFT.cdc"

// This script returns an array of NftViews for NFT Id

pub fun main(address: Address): [FreshArtNFT.NFTView] {
    let account = getAccount(address)
    let collectionRef = account.getCapability(FreshArtNFT.CollectionPublicPath).borrow<&{FreshArtNFT.FreshArtCollectionPublic}>()
        ?? panic("Could not borrow capability from public collection")
    return collectionRef.getNFTViews()
}


    pub fun main2(setId: UInt32): [UInt32]? {
      return FreshArtNFT.getArtIdsInSet(setId: setId)
    }