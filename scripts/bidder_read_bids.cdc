import Bidder from "../contracts/Bidder.cdc"
import FreshArtNFT from "../contracts/FreshArtNFT.cdc"

// For JS decoding NFT:
// https://docs.onflow.org/flow-js-sdk/packages/sdk/readme#custom-decoders

pub struct BidInfo {
    pub let key: UInt64
    pub let artId: UInt64?
    pub let price: UFix64?
    pub let metadata: {String: String}?
    init(key: UInt64, artId: UInt64?, price: UFix64?, metadata: {String: String}?) {
        self.key = key
        self.artId = artId
        self.price = price
        self.metadata = metadata
    }
}

// Reads the current asks owned by the owner
pub fun main(address: Address): [BidInfo] {
    let account = getAccount(address)
    let bidderRef = account.getCapability(Bidder.BidderPublicPath).borrow<&{Bidder.Public}>()
                    ?? panic("Could not borrow Bidder capability")
    let collectionRef = account.getCapability(FreshArtNFT.CollectionPublicPath).borrow<&{FreshArtNFT.FreshArtCollectionPublic}>()
                    ?? panic("Could not borrow capability from public collection")
    let bids = bidderRef.listBids()

    let infos: [BidInfo] = []

    for key in bids.keys {
      let info = BidInfo(key: key,
                         artId: bids[key]?.itemId,
                         price: bids[key]?.vault?.balance ?? 0.0,
                         metadata: bids[key]?.metadata)
      infos.append(info)
    }

    return infos
}
