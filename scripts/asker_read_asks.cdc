import Asker from "../contracts/Asker.cdc"
import FreshArtNFT from "../contracts/FreshArtNFT.cdc"

// For JS decoding NFT:
// https://docs.onflow.org/flow-js-sdk/packages/sdk/readme#custom-decoders

pub struct AskInfo {
    pub let key: UInt64
    pub let nftId: UInt64?
    pub let price: UFix64?
    pub(set) var metadata: {String: String}
    init(key: UInt64, nftId: UInt64?, price: UFix64?) {
        self.key = key
        self.nftId = nftId
        self.price = price
        self.metadata = {}
    }
}

// Reads the current asks owned by the owner
pub fun main(address: Address): [AskInfo] {
    let account = getAccount(address)
    let askerRef = account.getCapability(Asker.AskerPublicPath).borrow<&{Asker.Public}>()
                    ?? panic("Could not borrow Asker capability")
    let collectionRef = account.getCapability(FreshArtNFT.CollectionPublicPath).borrow<&{FreshArtNFT.FreshArtCollectionPublic}>()
                    ?? panic("Could not borrow capability from public collection")
    let asks = askerRef.listAsks()

    let infos: [AskInfo] = []

    for key in asks.keys {
        let info = AskInfo(key: key, nftId: asks[key]?.itemId, price: asks[key]?.price)

        // Attempt to get metadata for art
        if let id = info.nftId {
            if let art = collectionRef.borrowFreshArtNFT(id: id) {
                info.metadata = art.metadata
            }
        }

        infos.append(info)
    }

    return infos
}
