// Example from bluesign on discord
pub fun replaceAll(_ haystack:String, _ needle: String, _ replace: String): String{
  var index = indexOf(haystack, needle)
  if (index<0){
    return haystack
  }
  var prefix = haystack.slice(from: 0, upTo: index)
  var suffix = haystack.slice(from: index+needle.length, upTo: haystack.length)
  return prefix.concat(replace).concat(replaceAll(suffix, needle, replace))
}

pub fun indexOf(_ haystack:String, _ needle:String): Int{
  var h_index=0
  while (h_index<=haystack.length-needle.length){
    var substring = haystack.slice(from: h_index, upTo: h_index+needle.length)
    if (substring==needle) {
      return h_index
    }
    h_index=h_index+1
  }
  return -1
}