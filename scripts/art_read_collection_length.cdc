import NonFungibleToken from "../contracts/standard/NonFungibleToken.cdc"
import FreshArtNFT from "../contracts/FreshArtNFT.cdc"

// This script returns the size of an account's KittyItems collection.

pub fun main(address: Address): Int {
    let account = getAccount(address)
    let collectionRef = account.getCapability(FreshArtNFT.CollectionPublicPath).borrow<&{FreshArtNFT.FreshArtCollectionPublic}>()
        ?? panic("Could not borrow capability from public collection")    
    return collectionRef.getIDs().length
}
