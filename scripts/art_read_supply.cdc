import FreshArtNFT from "../contracts/FreshArtNFT.cdc"

pub fun main(): UInt64 {    
    return FreshArtNFT.totalSupply
}
