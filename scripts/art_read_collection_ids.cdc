import NonFungibleToken from "../contracts/standard/NonFungibleToken.cdc"
import FreshArtNFT from "../contracts/FreshArtNFT.cdc"

// This script returns an array of all the NFT IDs in an account's collection.

pub fun main(address: Address): [UInt64] {
    let account = getAccount(address)
    let collectionRef = account.getCapability(FreshArtNFT.CollectionPublicPath).borrow<&{FreshArtNFT.FreshArtCollectionPublic}>()
        ?? panic("Could not borrow capability from public collection")
    return collectionRef.getIDs()
}
