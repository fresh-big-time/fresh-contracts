import FreshDealer from "../contracts/FreshDealer.cdc"

// Fetches Gallery View from owner address
// Parameters:
// dealerAddress: Address for FreshDealer owner
// galleryId: Dealer gallery id
//
// pub struct GalleryView
//    pub let galleryId: UInt32
//    pub let isOpen: Bool
//    pub let prices: [UFix64]
//    pub let collections: {UInt32: [FreshArtNFT.NFTView]}

pub fun main(dealerAddress: Address, galleryId: UInt32): FreshDealer.GalleryView {
    let dealerAccount = getAccount(dealerAddress)
    let dealer = dealerAccount.getCapability(FreshDealer.DealerPublicPath).borrow<&{FreshDealer.Public}>()
            ?? panic("Could not borrow Dealer.")
    return dealer.getGalleryViewFor(galleryId: galleryId)
}
