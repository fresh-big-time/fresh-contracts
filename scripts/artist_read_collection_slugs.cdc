import FreshArtist from "../contracts/FreshArtist.cdc"

pub fun main(address: Address): [String] {
    let account = getAccount(address)
    let collectionRef = account.getCapability(FreshArtist.ArtistsPublicPath).borrow<&{FreshArtist.ArtistCollectionPublic}>()
        ?? panic("Could not borrow capability from public collection")
    return collectionRef.getSlugs()
}
 