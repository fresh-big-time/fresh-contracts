import NonFungibleToken from "../contracts/standard/NonFungibleToken.cdc"
import FreshArtNFT from "../contracts/FreshArtNFT.cdc"

// For JS decoding NFT:
// https://docs.onflow.org/flow-js-sdk/packages/sdk/readme#custom-decoders

pub fun main(address: Address, itemId: UInt64): &FreshArtNFT.NFT {
    let account = getAccount(address)
    let collectionRef = account.getCapability(FreshArtNFT.CollectionPublicPath).borrow<&{FreshArtNFT.FreshArtCollectionPublic}>()
        ?? panic("Could not borrow capability from public collection")    

    // borrow a reference to a specific NFT in the collection
    let art = collectionRef.borrowFreshArtNFT(id: itemId) ?? panic("Failed to borrow item from collection")

    return art
}
