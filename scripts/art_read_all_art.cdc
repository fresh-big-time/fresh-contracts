import FreshArtNFT from "../contracts/FreshArtNFT.cdc"

pub fun main(): [FreshArtNFT.Art] {
  return FreshArtNFT.getAllArtwork()
}