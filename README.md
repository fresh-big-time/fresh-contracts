# FRESH

## Flow Contracts

 This is a work in progress.

TESTNET DEPLOY
 `flow project deploy --network=testnet`
 `flow project deploy --network=testnet --update`
```
FreshArtNFT -> 0x1d4eb4552f566229 (af3dd091cbf82a21a6fc82b0e4a5797282489ab7218de54091a6d6b1b8fef665)

FreshMarket -> 0x1d4eb4552f566229 (3566a7f9f05c568b465b85f61c0d6a0fc9be43cfcb7d6338a790944becb92ab9)

FreshDealer -> 0x1d4eb4552f566229 (24ee431d153e3e48e832e8086bb6c8cb5d416f47e6faaa34b39c4f4d4dc58c47)
```
Remove a contract
 `flow accounts remove-contract FreshDealer --signer testnet-account -n testnet`

https://flow-view-source.com/testnet/account/0x1d4eb4552f566229

# Emulator
```
$ flow keys generate
Private Key      ac0fd519a90e1cf94bedb8fa743e887b016b6034e5c1ca697fbfe7163ca72fb8 
Public Key       6d46be13ec82c95581496c1254ebdb5b86cf092d0eb5b003ac4f9582686d6276e1a38c67b2d4be031fba674ce4e48a3c55adcb9efd76e30d8b9e1cc5a94b7bde 

Private Key      e0c697a145c8b112c6c4ad0c41bdaf6e3a1943b85358816fcdadb92b6ae91837 
Public Key       7f2381bcbad78cfa50bf2bd730d290feb60d03a4aeba60a2e1cf1a700b8549f54b30d9e120db14b2a341e96994158985c55dd1748234cab9e94656255427e784 
```
