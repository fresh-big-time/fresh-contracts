/*
 *    _______   _______     _______        _  ,   ____  ____
 *     | | \|    | | \ '.    | | \|     .|` '\|    | |  | |
 *     | |  '    | |  | |    | |  '    | |    |    | |  | |
 *     | |  ,    | |  | |    | |  ,    |  \        | |  | |
 *     | |-<|    | |-< <     | |-<|     \   '.     | |--| |
 *     | |  '    | |  | |    | |  '       '.  |    | |  | |
 *     | |       | |  | |,   | |  ,    |\   | /    | |  | |
 *    _|_|_     _|_|_  \_/  _|_|_/|    |'\_.|/    _|_|__|_|_
 *
 *    FRESHBIGTIME.COM
 *    Created by Tim Davidson, April 2021
 *
 *    Asker
 *
 *    Keeps NFTs for sale locally referencing a Collection. That part similar
 *    to TopShot Market. Give a AskToken with references back to sale item
 *    and FT payment receiver to the market.
 *
 *    Just one item per ask.
 *
 *    This does not store the NFT itself, like Bidder stores the Vault, because
 *    the owner is free to put up an NFT for sale other places. Market itself
 *    will need to enforce multiple postings.
 */

import FungibleToken from "./standard/FungibleToken.cdc"
import NonFungibleToken from "./standard/NonFungibleToken.cdc"

pub contract Asker {

  // Events
  pub event ContractInitialized()
  // TODO: Should Asker have events?
  // I feel like most Events should be implimented by whatever is using Asker.

  // Named Paths
  pub let AskerStoragePath: StoragePath
  pub let AskerPublicPath: PublicPath

  // ----------------------------------------------------------------
  // Resources Interfaces


  pub resource interface AskPublic {
    pub let price: UFix64
    pub let itemId: UInt64
  }

  // Public Capability used ONLY from within AskToken
  pub resource interface Public {
    // Lists outstanding Asks, indexed by key
    pub fun listAsks(): &{UInt64: Ask}
    access(contract) fun isAskStillActive(key: UInt64): Bool
    access(contract) fun cancelAsk(key: UInt64)
    access(contract) fun borrowItem(key: UInt64): &NonFungibleToken.NFT?
    access(contract) fun buyItem(key: UInt64): @NonFungibleToken.NFT
  }

  // Only owner can create Ask Tokens
  pub resource interface Owner {
    // Creates a Ask token referencing items in a collection
    //  itemId:             Item ids, must be NFT id in collection
    //  price:              $$ token price for item
    //  collection:         Cability for the collection the itemss are contained in
    //  paymentReceiver:    FT Receiver capability, for payment on success
    pub fun createAskToken(itemId: UInt64,
                           price: UFix64,
                           collection: Capability<&NonFungibleToken.Collection>,
                           paymentReceiver: Capability<&{FungibleToken.Receiver}>): @AskToken
    // Cancels a Ask locally only, repaying vault to repaymentReceiver
    pub fun cancelAsk(key: UInt64)
  }

  // ----------------------------------------------------------------
  // Resources

  // Token handed out to auction/market. It contains the functionality
  // to access the single item for sale in collection and pay for buy.
  //   key:     maps to asker capability's dictionary of sales
  //   itemId:  id for what item is for sale
  pub resource AskToken {
    pub let asker: Capability<&{Asker.Public}>
    pub let key: UInt64
    pub let itemId: UInt64
    pub let price: UFix64
    pub let paymentReceiver: Capability<&{FungibleToken.Receiver}>

    init(asker: Capability<&{Asker.Public}>,
         key: UInt64,
         itemId: UInt64,
         price: UFix64,
         paymentReceiver: Capability<&{FungibleToken.Receiver}>) {
      self.asker = asker
      self.key = key
      self.itemId = itemId
      self.price = price
      self.paymentReceiver = paymentReceiver
    }

    pub fun isAskStillActive(): Bool {
      let asker = self.asker.borrow() ?? panic("Could not borrow reference to Asker.")
      return asker.isAskStillActive(key: self.key)
    }

    pub fun cancelAsk() {
      let asker = self.asker.borrow() ?? panic("Could not borrow reference to Asker.")
      asker.cancelAsk(key: self.key)
    }

    pub fun borrowItem(): &NonFungibleToken.NFT? {
      let asker = self.asker.borrow() ?? panic("Could not borrow reference to Asker.")
      return asker.borrowItem(key: self.key)
    }

    pub fun buyItem(): @NonFungibleToken.NFT {
      let asker = self.asker.borrow() ?? panic("Could not borrow reference to Asker.")
      if !asker.isAskStillActive(key: self.key) { panic("Ask no longer active.") }
      let token <- asker.buyItem(key: self.key)
      return <- token
    }
  }

  // What this really does is store the collection capability locally
  // selectively exposing it to the AskToken
  pub resource Ask {
    pub let price: UFix64
    pub let itemId: UInt64
    access(contract) let collection: Capability<&NonFungibleToken.Collection>
    access(contract) let paymentReceiver: Capability<&{FungibleToken.Receiver}>

    init(price: UFix64,
         itemId: UInt64,
         collection: Capability<&NonFungibleToken.Collection>,
         paymentReceiver: Capability<&{FungibleToken.Receiver}>) {
      self.price = price
      self.itemId = itemId
      self.collection = collection
      self.paymentReceiver = paymentReceiver
    }
  }

  // Core Asker functionality.
  pub resource Base: Public, Owner {
    access(self) var asks: @{UInt64: Ask}
    access(self) var nextAskKey: UInt64

    init() {
      self.asks <- {}
      self.nextAskKey = 0
    }

    // Public

    pub fun listAsks(): &{UInt64: Ask} {
      return &self.asks as &{UInt64: Ask}
    }

    access(contract) fun isAskStillActive(key: UInt64): Bool {
      return self.asks[key] != nil
    }

    pub fun cancelAsk(key: UInt64) {
      pre {
        self.asks[key] != nil: "Ask no longer active."
      }
      let ask <- self.asks.remove(key: key)!
      destroy ask
    }

    access(contract) fun borrowItem(key: UInt64): &NonFungibleToken.NFT? {
      pre {
        self.asks[key] != nil: "Ask no longer active."
      }
      let askRef = &self.asks[key] as &Ask
      return askRef.collection.borrow()?.borrowNFT(id: askRef.itemId)
    }

    access(contract) fun buyItem(key: UInt64): @NonFungibleToken.NFT {
      pre {
        self.asks[key] != nil: "Ask no longer active."
      }
      let askRef = &self.asks[key] as &Ask
      let collectionRef = askRef.collection.borrow() ?? panic("Could not borrow reference to collection.")
      if (collectionRef.borrowNFT(id: askRef.itemId) == nil) {
        panic("No token matching this ID for sale!")
      }
      let token <- collectionRef.withdraw(withdrawID: askRef.itemId)

      let ask <- self.asks.remove(key: key)!
      destroy ask

      return <- token
    }

    // Owner
        
    pub fun createAskToken(itemId: UInt64,
                           price: UFix64,
                           collection: Capability<&NonFungibleToken.Collection>,
                           paymentReceiver: Capability<&{FungibleToken.Receiver}>): @AskToken {
      pre {
        collection.borrow()?.borrowNFT(id: itemId) != nil: "Collection and item must exist!"
        paymentReceiver.borrow() != nil: "Owner's FT Receiver Capability is invalid!"
      }
      // Only allow one Ask per itemId
      for id in self.asks.keys {
        if self.asks[id]?.itemId == itemId {
          panic("Item already listed for sale")
        }
      }

      // create Public capability from owner
      let asker = self.owner?.getCapability<&{Asker.Public}>(Asker.AskerPublicPath)
            ?? panic("Couldn't create Asker Capability from owner.")

      let key = self.nextAskKey
      self.nextAskKey = self.nextAskKey + (1 as UInt64)

      // Ask, with payment vault, stored locally
      let trash <- self.asks[key] <- create Ask(
         price: price,
         itemId: itemId,
         collection: collection,
         paymentReceiver: paymentReceiver
      )
      destroy trash

      // AskToken to be given to auction
      return <- create AskToken(
        asker: asker, key: key, itemId: itemId, price: price, paymentReceiver: paymentReceiver
      )
    }

    destroy() {
      destroy self.asks
    }
  }

  // ----------------------------------------------------------------
  // Functions

  pub fun new(): @Asker.Base {
    return <- create Base()
  }

  pub fun createAndStore() {
    self.account.save(<- create Base(), to: self.AskerStoragePath)
    self.account.link<&Base{Public}>(self.AskerPublicPath, target: self.AskerStoragePath)
  }

  init() {
    // Set our named paths
    self.AskerStoragePath = /storage/Asker
    self.AskerPublicPath = /public/Asker

    // Create a new Asker and store here
    self.account.save(<- create Base(), to: self.AskerStoragePath)
    self.account.link<&Base{Public}>(self.AskerPublicPath, target: self.AskerStoragePath)

    emit ContractInitialized()
  }

}