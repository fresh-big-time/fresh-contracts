import NonFungibleToken from "../contracts/standard/NonFungibleToken.cdc"
import FreshArtNFT from "../contracts/FreshArtNFT.cdc"
import FreshArtist from "../contracts/FreshArtist.cdc"

transaction {

  // Reference to the collection that will be receiving the NFT
  let receiverRef: &{FreshArtNFT.FreshArtCollectionPublic}
      
  // Reference to the Minter resource stored in account storage
  let minterRef: &FreshArtNFT.Minter

  // Reference to Artist that's making the artwork
  let artistRef: &FreshArtist.Artist
      
  prepare(account: AuthAccount) {
      self.receiverRef = account.getCapability<&{FreshArtNFT.FreshArtCollectionPublic}>(FreshArtNFT.CollectionPublicPath)
                             .borrow()
                             ?? panic("Could not borrow receiver reference")
      
      self.minterRef = account.borrow<&FreshArtNFT.Minter>(from: FreshArtNFT.MinterStoragePath)
                           ?? panic("could not borrow minter reference")

      let artistsRef = account.getCapability(FreshArtist.ArtistsPublicPath).borrow<&{FreshArtist.ArtistCollectionPublic}>()
        ?? panic("Could not borrow capability from public collection")

      self.artistRef = artistsRef.borrowArtistWithSlug(slug: "")
  }
      
  execute {
      // self.minterRef.mintNFT(recipient: self.receiverRef)

      log("NFT Minted and deposited to Collection")
  }
}