import FungibleToken from "../contracts/standard/FungibleToken.cdc"
import FreshArtist from "../contracts/FreshArtist.cdc"

transaction(recipient: Address, slug: String, name: String) {
  let admin: &FreshArtist.Admin
  let adminWallet: Capability<&{FungibleToken.Receiver}>

  prepare(account: AuthAccount) {

    //self.adminRef = account.getCapability<&FreshArtist.Admin>(FreshArtist.AdminStoragePath)
    self.admin = account.borrow<&FreshArtist.Admin>(from: FreshArtist.AdminStoragePath)
                  ?? panic("Could not borrow a reference to Admin")

    self.adminWallet = account.getCapability<&{FungibleToken.Receiver}>(/public/fusdReceiver)
  }

  execute {
    let recipientAccount = getAccount(recipient)
    
    let collectionRef = recipientAccount.getCapability(FreshArtist.ArtistsPublicPath)
    if (collectionRef == nil) {
      // collection doesn't exist yet? create it
      let collection <- FreshArtist.createEmptyCollection()
      recipientAccount.save(<-collection, to: FreshArtist.CollectionStoragePath)

      // create a public capability for the collection
      recipientAccount.link<&{ArtistCollectionPublic}>(
        self.ArtistsPublicPath,
        target: self.ArtistsStoragePath
      )

      collectionRef = recipientAccount.getCapability(FreshArtist.ArtistsPublicPath)
    }

    let artistWallet = recipientAccount.getCapability<&{FungibleToken.Receiver}>(/public/fusdReceiver)

    let royalties = {
      "minter": FreshArtist.Royalty(receiver: self.minterWallet, cut: 0.05 ),
      "artist": FreshArtist.Royalty(receiver: artistWallet, cut: 0.05 )
    }

    let artist <- self.admin.createArtist(
      recipient: collectionRef,
      slug: slug,
      name: name,
      metadata: {},
      royalties: royalties
    )

  }
}
