
 pub fun main(): [Fix64] {
  return prices(count: 10.0, lowPrice: 5.0, highPrice: 50.0, curve: 0.5)
}

pub fun prices(count: Fix64, lowPrice: Fix64, highPrice: Fix64, curve: Fix64): [Fix64] {
  let prices: [Fix64] = []
  let a = curve
  let b = (highPrice - lowPrice + (a * count * count)) / (1.0 - count)
  let c = highPrice - a - b

  var x = 1.0 as Fix64
  while x <= count {
    let y = (a*x*x) + (b*x) + c
    prices.append(y)
    x = x + 1.0
  }

  return prices
}

pub fun meh(value: UFix64): UFix64 {
  return UFix64(Int(value * UFix64(100)))/UFix64(100)
}




  /*
  post {
  getCurrentBlock().height >= self.randomBlock.height: "Naughty, try again..."
} */