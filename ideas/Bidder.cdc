/*
 *    _______   _______     _______        _  ,   ____  ____
 *     | | \|    | | \ '.    | | \|     .|` '\|    | |  | |
 *     | |  '    | |  | |    | |  '    | |    |    | |  | |
 *     | |  ,    | |  | |    | |  ,    |  \        | |  | |
 *     | |-<|    | |-< <     | |-<|     \   '.     | |--| |
 *     | |  '    | |  | |    | |  '       '.  |    | |  | |
 *     | |       | |  | |,   | |  ,    |\   | /    | |  | |
 *    _|_|_     _|_|_  \_/  _|_|_/|    |'\_.|/    _|_|__|_|_
 *
 *    FRESHBIGTIME.COM
 *    Created by Tim Davidson, April 2021
 *
 *    Bidder
 *
 *    This wraps a reference in a BidToken, keeping the vault with bidder,
 *    keep control and frree cancelling with the bidder.
 *
 *    Idea:
 *    All the users save a Bidder to their storage. When they make a bid,
 *    they user Vaults the bid $$ tokens into the Bidder Storage. Then create
 *    BidToken containing cabilities to the receiver and to request the vault.
 *    If they win then the auction/market can call BidToken.bidWins to access
 *    the vault and fulfill the winning to the collection. If they lose, or
 *    cancel their bid, then the vault is repayed to the  FT Receiver.
 *
 *    Made for FreshArtist, but hoping to do entirely standard so can be used by anybody.
 */

import FungibleToken from "./standard/FungibleToken.cdc"
import NonFungibleToken from "./standard/NonFungibleToken.cdc"

pub contract Bidder {

  // Events
  pub event ContractInitialized()
  // TODO: Should Bidder have events?
  // I feel like most Events should be implimented by whatever is using Bidder.

  // Named Paths
  pub let BidderStoragePath: StoragePath
  pub let BidderPublicPath: PublicPath

  // ----------------------------------------------------------------
  // Resources Interfaces

  // Public Capability used ONLY from within BidToken
  pub resource interface Public {
    // Lists outstanding bids, indexed by key
    pub fun listBids(): &{UInt64: Bid}
    access(contract) fun isBidStillActive(key: UInt64): Bool
    access(contract) fun getBidBalance(key: UInt64): UFix64
    access(contract) fun cancelBid(key: UInt64)
    access(contract) fun bidWins(key: UInt64): @FungibleToken.Vault
  }

  // Only owner can create Bid Tokens
  pub resource interface Owner {
    // Creates a bid token and stores vault with owner
    //  itemId:              id of single item being bid for. NFT id, or editions id.
    //  receiver:            NFT Collection receiver capability
    //  repaymentReceiver:   FT Receiver capability, for repayment on cancels
    //  vault:               FT Vault containing bid amount
    //  metadata             Optional display info, only for Owner reference NOT stored in BidToken
    pub fun createBidToken(itemId: UInt64,
                           receiver: Capability<&{NonFungibleToken.Receiver}>,
                           vault: @FungibleToken.Vault,
                           repaymentReceiver: Capability<&{FungibleToken.Receiver}>,
                           metadata: {String: String}?): @BidToken
    // Cancels bid locally only, repaying vault to repaymentReceiver
    pub fun cancelBid(key: UInt64)
  }

  // ----------------------------------------------------------------
  // Resources

  // Token handed out to auction/market. It contains the functionality
  // to access bid vault and receiver on wins, or cancel.
  // Bidder and receiver are seperate because they could be different sources.
  pub resource BidToken {
    pub let bidder: Capability<&{Bidder.Public}>
    pub let key: UInt64
    pub let price: UFix64 // check getBidderVaultBalance() for actual balance
    pub let itemId: UInt64
    pub let receiver: Capability<&{NonFungibleToken.Receiver}>

    init(bidder: Capability<&{Bidder.Public}>,
         key: UInt64,
         price: UFix64,
         itemId: UInt64,
         receiver: Capability<&{NonFungibleToken.Receiver}>) {
      self.key = key
      self.price = price
      self.itemId = itemId
      self.bidder = bidder
      self.receiver = receiver
    }

    pub fun isBidStillActive(): Bool {
      let bidder = self.bidder.borrow() ?? panic("Could not borrow reference to bidder.")
      return bidder.isBidStillActive(key: self.key)
    }

    pub fun getBidderVaultBalance(): UFix64 { // checks with bidder, instead of local variable
      let bidder = self.bidder.borrow() ?? panic("Could not borrow reference to bidder.")
      if !bidder.isBidStillActive(key: self.key) { panic("Bid no longer active.") }
      return bidder.getBidBalance(key: self.key)
    }

    pub fun cancelBid() {
      let bidder = self.bidder.borrow() ?? panic("Could not borrow reference to bidder.")
      bidder.cancelBid(key: self.key)
    }

    // Call this after depositing item in receiver
    pub fun bidWins(): @FungibleToken.Vault {
      let bidder = self.bidder.borrow() ?? panic("Could not borrow reference to bidder.")
      if !bidder.isBidStillActive(key: self.key) { panic("Bid no longer active.") }
      return <- bidder.bidWins(key: self.key)
    }
  }

  // Wrapper for vault stored with owner.
  pub resource Bid {
    pub let itemId: UInt64
    pub(set) var vault: @FungibleToken.Vault?
    access(contract) let receiverAddress: Address
    access(contract) let repaymentReceiver: Capability<&{FungibleToken.Receiver}>
    pub let metadata: {String: String}? // optionally store display info

    init(itemId: UInt64, vault: @FungibleToken.Vault,
         receiverAddress: Address, repaymentReceiver: Capability<&{FungibleToken.Receiver}>,
         metadata: {String: String}?) {
      self.itemId = itemId
      self.vault <- vault
      self.receiverAddress = receiverAddress
      self.repaymentReceiver = repaymentReceiver
      self.metadata = metadata
    }

    destroy() {
      destroy self.vault
    }
  }

  // Core Bidder functionality.
  pub resource Base: Public, Owner {
    access(self) var bids: @{UInt64: Bid}
    access(self) var nextBidKey: UInt64

    init() {
      self.bids <- {}
      self.nextBidKey = 0
    }

    // Public

    pub fun listBids(): &{UInt64: Bid} {
      return &self.bids as &{UInt64: Bid}
    }

    access(contract) fun isBidStillActive(key: UInt64): Bool {
      return self.bids[key] != nil
    }

    access(contract) fun getBidBalance(key: UInt64): UFix64 {
      return self.bids[key]?.vault?.balance ?? 0.0
    }

    pub fun cancelBid(key: UInt64) {
      pre {
        self.bids[key] != nil: "Bid no longer active."
      }
      let receiver = self.bids[key]?.repaymentReceiver?.borrow()! ?? panic("Could not borrow reference to repayment receiver.")
      let bid <- self.bids.remove(key: key)!
      // repay $$ tokens in vault
      if let vault <- bid.vault <- nil {
        receiver.deposit(from: <-vault)
      }
      destroy bid
    }

    access(contract) fun bidWins(key: UInt64): @FungibleToken.Vault {
      pre {
        self.bids[key] != nil: "Bid no longer active."
      }
      let bid <- self.bids.remove(key: key)!

      let vault <- bid.vault <- nil
      destroy bid

      // TODO: Record payment somewhere?

      return <- vault!
    }

    // Owner

    pub fun createBidToken(itemId: UInt64,
                           receiver: Capability<&{NonFungibleToken.Receiver}>,
                           vault: @FungibleToken.Vault,
                           repaymentReceiver: Capability<&{FungibleToken.Receiver}>,
                           metadata: {String: String}? ): @BidToken {
      let receiverRef = receiver.borrow() ?? panic("Could not borrow reference to receiver.")
      // create Public capability from owner
      let bidder = self.owner?.getCapability<&{Bidder.Public}>(Bidder.BidderPublicPath)
            ?? panic("Couldn't create Bidder Capability from owner.")

      let key = self.nextBidKey
      self.nextBidKey = self.nextBidKey + (1 as UInt64)

      let price = vault.balance

      // Bid, with payment vault, stored locally
      let trash <- self.bids[key] <- create Bid(
        itemId: itemId,
        vault: <- vault,
        receiverAddress: receiverRef.owner!.address,
        repaymentReceiver: repaymentReceiver,
        metadata: metadata
      )
      destroy trash

      // BidToken to be given to auction
      return <- create BidToken(
        bidder: bidder,
        key: key,
        price: price,
        itemId: itemId,
        receiver: receiver
      )
    }

    destroy() {
      // Attempt to reject each bid so $$ tokens not lost.
      let keys = self.bids.keys
      for key in keys {
        self.cancelBid(key: key)
      }
      destroy self.bids
    }
  }

  // ----------------------------------------------------------------
  // Functions

  pub fun new(): @Bidder.Base {
    return <- create Base()
  }

  pub fun createAndStore() {
    self.account.save(<- create Base(), to: self.BidderStoragePath)
    self.account.link<&Base{Public}>(self.BidderPublicPath, target: self.BidderStoragePath)
  }

  init() {
    // Set our named paths
    self.BidderStoragePath = /storage/Bidder
    self.BidderPublicPath = /public/Bidder

    // Create a new Bidder and store here
    self.account.save(<- create Base(), to: self.BidderStoragePath)
    self.account.link<&Base{Public}>(self.BidderPublicPath, target: self.BidderStoragePath)

    emit ContractInitialized()
  }

}