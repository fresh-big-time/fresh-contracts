/*
 *    _______   _______     _______        _  ,   ____  ____
 *     | | \|    | | \ '.    | | \|     .|` '\|    | |  | |
 *     | |  '    | |  | |    | |  '    | |    |    | |  | |
 *     | |  ,    | |  | |    | |  ,    |  \        | |  | |
 *     | |-<|    | |-< <     | |-<|     \   '.     | |--| |
 *     | |  '    | |  | |    | |  '       '.  |    | |  | |
 *     | |       | |  | |,   | |  ,    |\   | /    | |  | |
 *    _|_|_     _|_|_  \_/  _|_|_/|    |'\_.|/    _|_|__|_|_
 *
 *    FRESHBIGTIME.COM
 *    Created by Tim Davidson, April 2021
 *
 *    Fresh Artist
 *
 *    Reprents the artist on the platform. The artist contains the art.
 *    Each art is it's own market place with bids and asks.
 */

import NonFungibleToken from "./standard/NonFungibleToken.cdc"
import FungibleToken from "./standard/FungibleToken.cdc"
import Asker from "./Asker.cdc"
import Bidder from "./Bidder.cdc"
import FreshArtNFT from "./FreshArtNFT.cdc"

pub contract FreshArtist {

  // Events
  pub event ContractInitialized()
  pub event ArtistCreated(artistId: UInt64)
  pub event ArtistWithdraw(artistId: UInt64, from: Address?)
  pub event ArtistDeposit(artistId: UInt64, to: Address?)
  pub event ArtCreated(artistId: UInt64, artId: UInt64)

  pub event AskPosted(account: Address, artistId: UInt64, artId: UInt64, price: UFix64)
  pub event AskCanceled(account: Address, artistId: UInt64, artId: UInt64, price: UFix64)
  pub event BidPosted(account: Address, artistId: UInt64, artId: UInt64, price: UFix64)
  pub event BidCanceled(account: Address, artistId: UInt64, artId: UInt64, price: UFix64)
  pub event Sale(buyer: Address?, seller: Address?, artistId: UInt64, artId: UInt64, nftId: UInt64, price: UFix64)

  // Named Paths
  pub let ArtistsStoragePath: StoragePath
  pub let ArtistsPublicPath: PublicPath
  pub let AdminStoragePath: StoragePath

  // Variables
  pub var totalArtwork: UInt64
  pub var totalArtists: UInt64


  // ----------------------------------------------------------------
  // Resources Interfaces

  pub resource interface ArtPublic {
    pub var id: UInt64
    pub var artistId: UInt64
    pub var editions: {UInt64: Address} // nftId: Address
    pub var editionCount: UInt16 // 65,535
    pub var metadata: {String: String}
    pub var sales: [SaleForArt]
    pub var asks: @{UInt64: Asker.AskToken} // Asks are indexed by item/nft id, one per
    pub var bids: @{UInt64: Bidder.BidToken}
 
    pub fun postAsk(askToken: @Asker.AskToken) {
      pre {
        askToken.asker.check(): "Asker capability failed."
        askToken.paymentReceiver.check(): "Payment Receiver capability failed."
        self.editions[askToken.itemId] != nil: "NFT not found in Artist's artwork."
      }
    }

    pub fun cancelAsk(itemId: UInt64)

    pub fun buyAsk(itemId: UInt64,
                  receiver: Capability<&{NonFungibleToken.Receiver}>,
                  paymentTokens: @FungibleToken.Vault) {
      pre {
        self.editions[itemId] != nil: "NFT not found in Artist's artwork."
        self.asks[itemId] != nil: "Ask not available."
        receiver.check(): "Receiver capability failed."
        paymentTokens.balance >= (self.asks[itemId]?.price ?? 0.0): "Not enough tokens to buy the Ask!"
      }
    }


    pub fun postBid(bidToken: @Bidder.BidToken) {
      pre {
        bidToken.bidder.check(): "Asker capability failed."
        bidToken.receiver.check(): "Receiver capability failed."
      }
    }

    pub fun cancelBid(bidId: UInt64)

    pub fun sellToBid(bidId: UInt64, nftId: UInt64,
                      provider: Capability<&FreshArtNFT.Collection>,
                      paymentReceiver: Capability<&{FungibleToken.Receiver}>) {
      pre {
        self.editions[nftId] != nil: "NFT not found in Artist's artwork."
        self.bids[bidId] != nil: "Bid not available."
        provider.check(): "Provider capability failed."
        paymentReceiver.check(): "Payment Receiver capability failed."
      }
    }
  }

  pub resource interface ArtistPublic {
    pub var id: UInt64
    pub var slug: String
    pub var name: String
    pub var metadata: {String: String}

    pub fun deposit(art: @Art)
    pub fun getArtworkIds(): [UInt64]
    pub fun borrowArt(id: UInt64): &{ArtPublic} {
      post {
        result.id == id: "Cannot borrow Art reference: The ID of the returned reference is incorrect"
      }
    }
  }

  // A public interface to allow others to see Artists
  pub resource interface ArtistCollectionPublic {
    pub fun deposit(artist: @Artist)
    pub fun getArtistIds(): [UInt64]
    pub fun borrowArtist(id: UInt64): &{ArtistPublic} {
      post {
        result.id == id: "Cannot borrow Artist reference: The ID of the returned reference is incorrect"
      }
    }
    pub fun borrowArtistWithSlug(slug: String): &{ArtistPublic} {
      post {
        result.slug == slug: "Cannot borrow Artist reference: The ID of the returned reference is incorrect"
      }
    }
  }

  // ----------------------------------------------------------------
  // Structs

  // Royalty fees on a sale to artist, minter
  pub struct Royalty {
    pub let receiver:Capability<&{FungibleToken.Receiver}> 
    pub let cut: UFix64

    init(receiver:Capability<&{FungibleToken.Receiver}>, cut: UFix64 ){
      self.receiver = receiver
      self.cut = cut
    }
  }

  // Sale record stored on Art
  pub struct SaleForArt {
    pub let nftId: UInt64
    pub let price: UFix64
    pub let buyer: Address
    pub let seller: Address
    pub let time: Fix64
    
    init(nftId: UInt64, price: UFix64, buyer: Address, seller: Address, time: Fix64) {
      self.nftId = nftId
      self.price = price
      self.buyer = buyer
      self.seller = seller
      self.time = time
    }
  }

  // Sale record also stored for Artist, but less info
  pub struct SaleForArtist {
    pub let artId: UInt64
    pub let price: UFix64
    pub var editionCount: UInt16 // 65,535
    pub let time: Fix64

    init(artId: UInt64, price: UFix64, editionCount: UInt16, time: Fix64) {
      self.artId = artId
      self.price = price
      self.editionCount = editionCount
      self.time = time
    }
  }

  // ----------------------------------------------------------------
  // Resources

  pub resource Art: ArtPublic {
    pub var id: UInt64
    pub var artistId: UInt64
    pub var editions: {UInt64: Address} // nftId: Address
    pub var editionCount: UInt16 // 65,535
    pub var metadata: {String: String}
    pub var sales: [SaleForArt]
    pub var asks: @{UInt64: Asker.AskToken} // Asks are indexed by item/nft id, one per
    pub var bids: @{UInt64: Bidder.BidToken} // Bids are indexed by nextBidIndex
    // Owner
    pub var royalties: {String: Royalty}
    priv var nextBidIndex: UInt64
    
    init(id: UInt64, artistId: UInt64, editionCount: UInt16, metadata: {String: String}, royalties: {String: Royalty}) {
      self.id = id
      self.artistId = artistId
      self.editionCount = editionCount
      self.metadata = metadata
      self.royalties = royalties
      self.editions = {}
      self.sales = []
      self.asks <- {}
      self.bids <- {}
      self.nextBidIndex = 0
    }

    pub fun postAsk(askToken: @Asker.AskToken) {
      let address = askToken.asker.borrow()?.owner?.address ?? panic("Could not find Address of Asker")
      let price = askToken.price
      let trash <- self.asks[askToken.itemId] <- askToken
      destroy trash
      emit AskPosted(account: address, artistId: self.artistId, artId: self.id, price: price)
    }

    pub fun cancelAsk(itemId: UInt64) {
      let askToken <- self.asks.remove(key: itemId)  ?? panic("Missing Ask")
      let address = askToken.asker.borrow()?.owner?.address ?? panic("Could not find Address of Asker")
      askToken.cancelAsk()
      emit AskCanceled(account: address, artistId: self.artistId, artId: self.id, price: askToken.price)
      destroy askToken
    }

    pub fun buyAsk(itemId: UInt64,
                   receiver: Capability<&{NonFungibleToken.Receiver}>,
                   paymentTokens: @FungibleToken.Vault) {
      let askToken <- self.asks.remove(key: itemId) ?? panic("Ask not found.")
      self.completeSale(nftId: itemId, 
                        price: askToken.price,
                        token: <- askToken.buyItem(),
                        receiver: receiver,
                        paymentTokens: <- paymentTokens,
                        paymentReceiver: askToken.paymentReceiver)
      destroy askToken
    }


    // Post bid. Ensure all bid tokens have the same receiver
    pub fun postBid(bidToken: @Bidder.BidToken) {
      if bidToken.getBidderVaultBalance() != bidToken.price {
        panic("Bid's vault balance must be equal to bid's price.")
      }
      let address = bidToken.bidder.borrow()?.owner?.address ?? panic("Could not find Address of Bidder")
      let price = bidToken.price
      let trash <- self.bids[self.nextBidIndex] <- bidToken
      destroy trash
      self.nextBidIndex = self.nextBidIndex + (1 as UInt64)
      emit BidPosted(account: address, artistId: self.artistId, artId: self.id, price: price)
    }

    pub fun cancelBid(bidId: UInt64) {
      let bidToken <- self.bids.remove(key: bidId) ?? panic("Missing Bid!")
      let address = bidToken.bidder.borrow()?.owner?.address ?? panic("Could not find Address of Asker")
      bidToken.cancelBid()
      emit AskCanceled(account: address, artistId: self.artistId, artId: self.id, price: bidToken.price)
      destroy bidToken
    }

    pub fun sellToBid(bidId: UInt64, nftId: UInt64, provider: Capability<&FreshArtNFT.Collection>, paymentReceiver: Capability<&{FungibleToken.Receiver}>) {
      let bidToken <- self.bids.remove(key: bidId) ?? panic("Bid not found.")
      let provider = provider.borrow() ?? panic("Provider capability failed.")
      let token <-provider.withdraw(withdrawID: nftId)

      let rec: Capability<&{NonFungibleToken.Receiver}> = bidToken.receiver

      self.completeSale(nftId: nftId, 
                        price: bidToken.price,
                        token: <- token,
                        receiver: bidToken.receiver,
                        paymentTokens: <- bidToken.bidWins(),
                        paymentReceiver: paymentReceiver)

      destroy bidToken
    }


    priv fun completeSale(nftId: UInt64,
                          price: UFix64,
                          token: @NonFungibleToken.NFT,
                          receiver: Capability<&{NonFungibleToken.Receiver}>,
                          paymentTokens: @FungibleToken.Vault,
                          paymentReceiver: Capability<&{FungibleToken.Receiver}>) {
      pre { // Should be covered by public interface method preconditions, but certain
        self.editions[nftId] != nil: "NFT not found in Artist's artwork."
        paymentTokens.balance >= price: "Not enough tokens to buy the NFT!"
      }
      let receiver = receiver.borrow() ?? panic("Receiver capability failed.")
      let paymentReceiver = paymentReceiver.borrow() ?? panic("Payment Receiver capability failed.")

      // Subtract royalty fees
      for key in self.royalties.keys {
        let royalty = self.royalties[key]!
        let amount = price * royalty.cut
        if let receiver = royalty.receiver.borrow() {
          receiver.deposit(from: <- paymentTokens.withdraw(amount: amount) )
        }
      }

      // Deposit $$ tokens in paymentReceiver
      paymentReceiver.deposit(from: <- paymentTokens)

      // Deposit the NFT in bidder's collection
      receiver.deposit(token: <- token)

      let seller = paymentReceiver.owner?.address
      let buyer = receiver.owner?.address

      // update editions
      self.editions[nftId] = buyer
      emit Sale(buyer: buyer, seller: seller,
                    artistId: self.artistId, artId: self.id, nftId: nftId, price: price)
    }

    destroy() {
      for key in self.asks.keys { self.asks[key]?.cancelAsk() }
      destroy self.asks
      for key in self.bids.keys { self.bids[key]?.cancelBid() }
      destroy self.bids
    }
  }



  pub resource Artist: ArtistPublic {
    pub var id: UInt64
    pub var slug: String
    pub var name: String
    pub var metadata: {String: String}
    priv var royalties: {String: Royalty}
    priv var artwork: @{UInt64: Art} // artId: Art

    init(id: UInt64, slug: String, name: String, metadata: {String: String}, royalties: {String: Royalty}) {
      self.id = id
      self.slug = slug
      self.name = name
      self.metadata = metadata
      self.royalties = royalties
      self.artwork <- {}
    }


    pub fun withdraw(withdrawID: UInt64): @Art {
      let art <- self.artwork.remove(key: withdrawID) ?? panic("missing Artwork")
      return <-art
    }

    pub fun deposit(art: @Art) {
      let id: UInt64 = art.id
      let trash <- self.artwork[id] <- art
      destroy trash
    }

    pub fun getArtworkIds(): [UInt64] {
      return self.artwork.keys
    }

    pub fun borrowArt(id: UInt64): &{ArtPublic} {
      return &self.artwork[id] as &{ArtPublic}
    }

    pub fun mint(
        minter: &FreshArtNFT.Minter,
        recipient: &{FreshArtNFT.FreshArtCollectionPublic},
        editionCount: UInt16, url: String?,
        mediaType: String?, dataType: String?, dataSize: UInt64?, data: String?,
        metadata: {String: String}) {

      let artId = FreshArtist.totalArtwork
      FreshArtist.totalArtwork = FreshArtist.totalArtwork + (1 as UInt64)

      if metadata["artist"] == nil {
        metadata["artist"] = self.name
      }

      // Mint the NFTs using the Minter
      var ntfCollection <- minter.batchMintNFT(
        editionCount: editionCount,
        artId: artId,
        url: url,
        mediaType: mediaType,
        dataType: dataType,
        dataSize: dataSize,
        data: data,
        metadata: metadata
      )

      // Store data about these minted editions in an Art resource
      let art <- create Art(
        id: artId, artistId: self.id, editionCount: editionCount,
        metadata: metadata, royalties: self.royalties
      )

      // store references to minter NFTs as editions owned by recipient
      let nftIds = ntfCollection.getIDs()
      while nftIds.length > 0 {
        art.editions[nftIds.removeFirst()] = recipient.getAddress()
      }

      // Store artwork in this Artist
      let trash <- self.artwork[artId] <- art
      destroy trash

      // deposit it in the recipient's account using their reference
      recipient.batchDeposit(tokens: <-ntfCollection)
    }

    destroy() {
      destroy self.artwork
    }
  }



  pub resource Collection: ArtistCollectionPublic {
    pub var artists: @{String: Artist} // slug: Artist

    init () {
      self.artists <- {}
    }

    pub fun withdraw(slug: String): @Artist {
      let artist <- self.artists.remove(key: slug) ?? panic("missing Artist")
      emit ArtistWithdraw(artistId: artist.id, from: self.owner?.address)
      return <-artist
    }

    pub fun deposit(artist: @Artist) {
      let slug: String = artist.slug
      emit ArtistDeposit(artistId: artist.id, to: self.owner?.address)
      let trash <- self.artists[slug] <- artist
      destroy trash
    }

    pub fun getArtistIds(): [UInt64] {
      var ids: [UInt64] = []
      for key in self.artists.keys {
        if let id = self.artists[key]?.id {
          ids.append(id)
        }
      }
      return ids
    }

    pub fun getArtistSlugs(): [String] {
      return self.artists.keys
    }

    pub fun borrowArtist(id: UInt64): &{ArtistPublic} {
      for key in self.artists.keys {
        if self.artists[key]?.id == id {
          return &self.artists[key] as &{ArtistPublic}
        }
      }
      panic("Artist with id not found")
    }

    pub fun borrowArtistWithSlug(slug: String): &{ArtistPublic} {
      return &self.artists[slug] as &{ArtistPublic}
    }

    destroy() {
      destroy self.artists
    }
  }

  pub resource Admin {
    pub fun createArtist(recipient: &{ArtistCollectionPublic}, slug: String, name: String,
                         metadata: {String: String}, royalties: {String: Royalty}) {
      var newArtist <- create Artist(
        id: FreshArtist.totalArtists,
        slug: slug,
        name: name,
        metadata: metadata,
        royalties: royalties
      )
      recipient.deposit(artist: <-newArtist)
      FreshArtist.totalArtists = FreshArtist.totalArtists + (1 as UInt64)
    }
    pub fun createAdmin(): @Admin {
      return <-create Admin()
    }
  }

  // ----------------------------------------------------------------
  // Functions

  // public function that anyone can call to create a new empty collection
  pub fun createEmptyCollection(): @FreshArtist.Collection {
    return <- create Collection()
  }


  // ----------------------------------------------------------------
  init() {
    self.totalArtwork = 0
    self.totalArtists = 0
    self.ArtistsStoragePath = /storage/FreshArtists
    self.ArtistsPublicPath = /public/FreshArtists
    self.AdminStoragePath = /storage/FreshArtistsAdmin

    // Create a Collection resource and save it to storage
    let collection <- create Collection()
    self.account.save(<-collection, to: self.ArtistsStoragePath)

    // create a public capability for the collection
    self.account.link<&{ArtistCollectionPublic}>(
      self.ArtistsPublicPath,
      target: self.ArtistsStoragePath
    )

    // Create an admin and store here
    let admin <- create Admin()
    self.account.save(<-admin, to: self.AdminStoragePath)

    emit ContractInitialized()
  }


}