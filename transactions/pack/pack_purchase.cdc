import FungibleToken from "../../contracts/standard/FungibleToken.cdc"
import PackMarket from "../../contracts/PackMarket.cdc"

transaction(marketAddress: Address, packId: UInt32) {
  let market: &{PackMarket.Public}
  let accountAddress: Address
  let paymentTokens: @FungibleToken.Vault

  prepare(account: AuthAccount) {
    let marketAccount = getAccount(marketAddress)
    self.market = marketAccount.getCapability(PackMarket.PublicPath).borrow<&{PackMarket.Public}>()
            ?? panic("Could not borrow Pack Market.")
    self.accountAddress = account.address

    let price = self.market.getPrice(packId: packId)

    // borrow a reference to the signer's fungible token Vault
    //let provider = account.borrow<&DapperUtilityCoin.Vault{FungibleToken.Provider}>(from: /storage/dapperUtilityCoinVault)!
    let provider = account.borrow<&FungibleToken.Vault{FungibleToken.Provider}>(from: /storage/flowToken)
          ?? panic("Could not borrow account's FT provider.")

          // TODO: Where are the tokens!??

    // withdraw tokens from the signer's vault
    //self.paymentTokens <- provider.withdraw(amount: purchaseAmount) as! @DapperUtilityCoin.Vault
    self.paymentTokens <- provider.withdraw(amount: price)

  }

  execute {
    self.market.purchase(
      packId: packId,
      buyerAddress: self.accountAddress,
      paymentTokens: <- self.paymentTokens
    )
  }
}