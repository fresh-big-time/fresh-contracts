import PackMarket from "../../contracts/PackMarket.cdc"

// Cancels an entire pack

transaction(packId: UInt32) {
  let marketRef: &PackMarket.Market

  prepare(account: AuthAccount) {
    self.marketRef = account.borrow<&PackMarket.Market>(from: PackMarket.StoragePath) ?? panic("No Market resource in storage")
  }

  execute {
    self.marketRef.cancelPack(packId: packId)
  }
}