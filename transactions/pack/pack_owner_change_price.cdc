import PackMarket from "../../contracts/PackMarket.cdc"

// Changes pack price

transaction(packId: UInt32, price: UFix64) {
  let marketRef: &PackMarket.Market

  prepare(account: AuthAccount) {
    self.marketRef = account.borrow<&PackMarket.Market>(from: PackMarket.StoragePath) ?? panic("No Market resource in storage")
  }

  execute {
    self.marketRef.setPrice(packId: packId, price: price)
  }
}