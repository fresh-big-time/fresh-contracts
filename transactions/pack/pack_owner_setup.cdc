import FungibleToken from "../../contracts/standard/FungibleToken.cdc"
import PackMarket from "../../contracts/PackMarket.cdc"

// PackMarket creates a Market and public cpabaility in te contract owner's account
// at /public/FreshPackMarket, target: /storage/FreshPackMarket
// Sets up payment for contract owner account.

transaction {
  let marketRef: &PackMarket.Market
  let accountWallet: Capability<&{FungibleToken.Receiver}>

  prepare(account: AuthAccount) {
    self.marketRef = account.borrow<&PackMarket.Market>(from: PackMarket.StoragePath) ?? panic("No Market resource in storage")
    self.accountWallet = account.getCapability<&{FungibleToken.Receiver}>(/public/flowTokenReceiver)
  }

  execute {
    self.marketRef.setPaymentReceiver(self.accountWallet)
  }
}