import PackMarket from "../../contracts/PackMarket.cdc"

// Adds a new pack. With no royalties, all proceeds go to Market

transaction(price: UFix64, available: UInt32) {
  let marketRef: &PackMarket.Market
  let currentPackId: UInt32

  prepare(account: AuthAccount) {
    self.marketRef = account.borrow<&PackMarket.Market>(from: PackMarket.StoragePath) ?? panic("No Market resource in storage")
    self.currentPackId = PackMarket.nextPackId
  }

  execute {
    self.marketRef.createPack(price: price, available: available, royalties: {})
  }

  post {
    self.marketRef.getAvailable(packId: self.currentPackId) == available:
          "Could not find created pack id with matching available."
  }
}