import FreshArtNFT from "../../contracts/FreshArtNFT.cdc"

// Send the art in a "pack" to a user's collection

// Parameters:
//
// recipientAddress: the Flow address of the account receiving the pack
// artIds: an array of art IDs to be withdrawn from the owner's collection

transaction(recipientAddress: Address, artIds: [UInt64]) {

    prepare(acct: AuthAccount) {        
        // get the recipient's public account object
        let recipient = getAccount(recipientAddress)

        // get the Collection reference for the receiver
        let receiverRef = recipient.getCapability(FreshArtNFT.CollectionPublicPath).borrow<&{FreshArtNFT.CollectionPublic}>()
            ?? panic("Cannot borrow a reference to the recipient's collection")

        let collection = acct.borrow<&FreshArtNFT.Collection>(from: FreshArtNFT.CollectionStoragePath)
            ?? panic("Cannot borrow a reference to the collection")

        // Deposit the pack of art to the recipient's collection
        receiverRef.batchDeposit(tokens: <- collection.batchWithdraw(ids: artIds))
    }
}