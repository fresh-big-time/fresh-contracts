import FungibleToken from "../contracts/standard/FungibleToken.cdc"
import NonFungibleToken from "../contracts/standard/NonFungibleToken.cdc"
import FreshArtNFT from "../contracts/FreshArtNFT.cdc"
import FreshDealer from "../contracts/FreshDealer.cdc"

transaction(dealerAddress: Address, galleryId: UInt32, artIds: [UInt32]) {
  let dealer: &{FreshDealer.Public}
  let receiver: Capability<&{NonFungibleToken.Receiver}>
  let paymentTokens: @FungibleToken.Vault
  let reimburseReceiver: Capability<&{FungibleToken.Receiver}>

  prepare(account: AuthAccount) {
    let dealerAccount = getAccount(dealerAddress)
    self.dealer = dealerAccount.getCapability(FreshDealer.DealerPublicPath).borrow<&{FreshDealer.Public}>()
            ?? panic("Could not borrow Dealer.")

    // Account's collection receiver capability, to receive NFT when fulfilled.
    self.receiver = account.getCapability<&{NonFungibleToken.Receiver}>(FreshArtNFT.CollectionPublicPath)

    // Account's FT receiver capability, to get payment back if reimbured instead.
    self.reimburseReceiver = account.getCapability<&{FungibleToken.Receiver}>(/public/flowTokenReceiver)

    // Get price for count
    let price = self.dealer.getPriceFor(galleryId: galleryId, count: artIds.length)

    // borrow a reference to the signer's fungible token Vault
    //let provider = account.borrow<&DapperUtilityCoin.Vault{FungibleToken.Provider}>(from: /storage/dapperUtilityCoinVault)!
    let provider = account.borrow<&FungibleToken.Vault{FungibleToken.Provider}>(from: /storage/flowToken)
          ?? panic("Could not borrow account's FT provider.")

          // TODO: switch to USD?

    // withdraw tokens from the signer's vault
    //self.paymentTokens <- provider.withdraw(amount: purchaseAmount) as! @DapperUtilityCoin.Vault
    self.paymentTokens <- provider.withdraw(amount: price)

  }

  execute {
    self.dealer.purchase(
        galleryId: galleryId,
        artIds: artIds, 
        receiver: self.receiver,
        paymentTokens: <- self.paymentTokens,
        reimburseReceiver: self.reimburseReceiver
    )
  }
}