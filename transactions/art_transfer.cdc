import NonFungibleToken from "../contracts/standard/NonFungibleToken.cdc"
import FreshArtNFT from "../contracts/FreshArtNFT.cdc"

transaction(recipientAddress: Address, withdrawID: UInt64) {

  // The field that will hold the NFT as it is being
  // transferred to the other account
    let transferToken: @NonFungibleToken.NFT

  prepare(account: AuthAccount) {
    // Borrow a reference from the stored collection
    let collectionRef = account.borrow<&FreshArtNFT.Collection>(from: FreshArtNFT.CollectionStoragePath)
       ?? panic("Could not borrow a reference to the owner's collection")
      
    // Call the withdraw function on the sender's Collection
    // to move the NFT out of the collection
    self.transferToken <- collectionRef.withdraw(withdrawID: withdrawID)
  }

  execute {
    // Get the recipient's public account object
    let recipient = getAccount(recipientAddress)

    // get the Collection reference for the receiver
    let receiverRef = recipient.getCapability(FreshArtNFT.CollectionPublicPath).borrow<&{FreshArtNFT.CollectionPublic}>()
            ?? panic("Cannot borrow a reference to the recipient's collection")
      
    // Deposit the NFT in the receivers collection
    receiverRef.deposit(token: <-self.transferToken)
  }
}