import FreshArtNFT from "../contracts/FreshArtNFT.cdc"

transaction {
  prepare(account: AuthAccount) {
      // Create a new empty collection
      let collection <- FreshArtNFT.createEmptyCollection()

      // store the empty NFT Collection in account storage
      account.save<@FreshArtNFT.Collection>(<-collection, to: FreshArtNFT.CollectionStoragePath)

      // create a public capability for the Collection
      account.link<&{FreshArtNFT.CollectionPublic}>(
        FreshArtNFT.CollectionPublicPath,
        target: FreshArtNFT.CollectionStoragePath
      )
  }
}