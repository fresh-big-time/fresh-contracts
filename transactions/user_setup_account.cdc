import NonFungibleToken from "../contracts/standard/NonFungibleToken.cdc"
import FreshArtNFT from "../contracts/FreshArtNFT.cdc"

transaction {
  prepare(account: AuthAccount) {
    // if the account doesn't already have a collection
    if account.borrow<&FreshArtNFT.Collection>(from: FreshArtNFT.CollectionStoragePath) != nil {
      return
    }

    // Create a new empty collection
    let collection <- FreshArtNFT.createEmptyCollection()
      
    // store the empty NFT Collection in account storage
    account.save<@FreshArtNFT.Collection>(<-collection, to: FreshArtNFT.CollectionStoragePath)
      
    log("Collection created for account 1")
      
    // create a public capability for the Collection
    account.link<&{FreshArtNFT.CollectionPublic}>(
      FreshArtNFT.CollectionPublicPath,
      target: FreshArtNFT.CollectionStoragePath
    )

    log("Capability created")
  }
}