import FreshArtNFT from "../../contracts/FreshArtNFT.cdc"

// This transaction mints multiple collectibles from an art
// from a single set/play combination (otherwise known as edition)

// Parameters:
//
// setID: the ID of the set to be minted from
// artId: the ID of the art to mint from
// quantity: the number of collectibles to be minted
// recipientAddress: the Flow address of the account receiving the collection

transaction(setId: UInt32, artId: UInt32, quantity: UInt64, recipientAddress: Address) {
    let adminRef: &FreshArtNFT.Admin

    prepare(acct: AuthAccount) {
        self.adminRef = acct.borrow<&FreshArtNFT.Admin>(from: FreshArtNFT.AdminStoragePath)
            ?? panic("Could not borrow a reference to the Admin resource")
    }

    execute {
        // Borrow a reference to the specified set
        let setRef = self.adminRef.borrowSet(setId: setId)

        // Mint all the new NFTs
        let collection <- setRef.batchMintCollectible(artId: artId, quantity: quantity)

        // get the public account object for the recipient
        let recipient = getAccount(recipientAddress)

        // get the Collection reference for the receiver
        let receiverRef = recipient.getCapability(FreshArtNFT.CollectionPublicPath).borrow<&{FreshArtNFT.CollectionPublic}>()
            ?? panic("Cannot borrow a reference to the recipient's collection")

        // deposit the NFT in the receivers collection
        receiverRef.batchDeposit(tokens: <- collection)
    }
}