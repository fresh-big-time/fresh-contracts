import FreshArtNFT from "../../contracts/FreshArtNFT.cdc"

// This transaction is for the admin to create a new set resource
// and store it in the top shot smart contract

// Parameters:
//
// setName: the name of a new Set to be created
// setDescription: optional string description of set

transaction(setName: String, setDescription: String) {
    let adminRef: &FreshArtNFT.Admin
    let currentSetID: UInt32

    prepare(acct: AuthAccount) {
        // borrow a reference to the Admin resource in storage
        self.adminRef = acct.borrow<&FreshArtNFT.Admin>(from: FreshArtNFT.AdminStoragePath)
            ?? panic("Could not borrow a reference to the Admin resource")
        self.currentSetID = FreshArtNFT.nextSetId
    }

    execute {        
        // Create a set with the specified name
        self.adminRef.createSet(name: setName, description: setDescription)
    }

    post {
        FreshArtNFT.getSetName(setId: self.currentSetID) == setName: "Could not find the specified set"
    }
}