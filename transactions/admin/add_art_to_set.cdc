import FreshArtNFT from "../../contracts/FreshArtNFT.cdc"

transaction(setId: UInt32, artId: UInt32) {
    let adminRef: &FreshArtNFT.Admin
    
    prepare(acct: AuthAccount) {
        self.adminRef = acct.borrow<&FreshArtNFT.Admin>(from: FreshArtNFT.AdminStoragePath)
            ?? panic("Could not borrow a reference to the Admin resource")
    }
    execute {
        let setRef = self.adminRef.borrowSet(setId: setId)
        setRef.addArt(artId: artId)
    }
    post {
        FreshArtNFT.getArtIdsInSet(setId: setId)!.contains(artId): "set does not contain playID"
    }
}