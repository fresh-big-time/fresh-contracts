import FreshDealer from "../../contracts/FreshDealer.cdc"

// Allows Dealer owner to open or close a gallery
// Parameters:
// galleryId: Dealer gallery id
// isOpen: true to open, false to close

transaction(galleryId: UInt32, isOpen: Bool) {
    let dealerRef: &FreshDealer.Dealer

    prepare(acct: AuthAccount) {
        self.dealerRef = acct.borrow<&FreshDealer.Dealer>(from: FreshDealer.DealerStoragePath)
            ?? panic("Could not borrow a reference to the Dealer resource")
    }

    execute {
        self.dealerRef.setOpenForSale(galleryId: galleryId, isOpen: isOpen)
    }
}