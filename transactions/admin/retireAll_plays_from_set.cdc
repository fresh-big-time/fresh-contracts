import FreshArtNFT from "../../contracts/FreshArtNFT.cdc"

// Retired art can no longer mint collectibles

// Parameters:
// 
// setID: the ID of the set to be retired entirely

transaction(setId: UInt32, artId: UInt32) {
    let adminRef: &FreshArtNFT.Admin

    prepare(acct: AuthAccount) {
        self.adminRef = acct.borrow<&FreshArtNFT.Admin>(from: FreshArtNFT.AdminStoragePath)
            ?? panic("Could not borrow a reference to the Admin resource")
    }
    execute {
        let setRef = self.adminRef.borrowSet(setId: setId)
        setRef.retireAll()
    }
}