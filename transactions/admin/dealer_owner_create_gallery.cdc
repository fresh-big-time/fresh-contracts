import FreshDealer from "../../contracts/FreshDealer.cdc"
import FungibleToken from "../../contracts/standard/FungibleToken.cdc"

// Allows Dealer owner to create a gallery. Gallery is empty and closed.
// Parameters:
// galleryId: Dealer gallery id

// TODO: Switch FT to particular, USD??

transaction(lowPrice: UFix64, highPrice: UFix64) {
    let dealerRef: &FreshDealer.Dealer
    let paymentReceiver: Capability<&{FungibleToken.Receiver}>
    let currentGalleryId: UInt32

    prepare(acct: AuthAccount) {
        self.currentGalleryId = FreshDealer.nextGalleryId;
        self.dealerRef = acct.borrow<&FreshDealer.Dealer>(from: FreshDealer.DealerStoragePath)
            ?? panic("Could not borrow a reference to the Dealer resource")
        self.paymentReceiver = acct.getCapability<&{FungibleToken.Receiver}>(/public/flowTokenReceiver)
    }

    execute {
        self.dealerRef.createGallery(
            paymentReceiver: self.paymentReceiver,
            lowPrice: lowPrice,
            highPrice: highPrice
        )
    }

    post {        
        self.dealerRef.borrowGallery(galleryId: self.currentGalleryId) != nil: "galleryId doesn't exist"
    }
}