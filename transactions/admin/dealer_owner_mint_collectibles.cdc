import FreshArtNFT from "../../contracts/FreshArtNFT.cdc"
import FreshDealer from "../../contracts/FreshDealer.cdc"

// Account must be a admin for both, FreshArtNFT admin and FreshDealer owner

// Parameters:
//
// setId: the ID of a set containing the target play
// artId: the ID of a art to mint from
// quantity: the number of collectibles to be minted
// galleryId: Dealer gallery id to deposit NFTs in

transaction(setId: UInt32, artId: UInt32, quantity: UInt64, galleryId: UInt32) {
    let adminRef: &FreshArtNFT.Admin
    let dealerRef: &FreshDealer.Dealer

    prepare(acct: AuthAccount) {
        self.adminRef = acct.borrow<&FreshArtNFT.Admin>(from: FreshArtNFT.AdminStoragePath)
            ?? panic("Could not borrow a reference to the Admin resource")
        self.dealerRef = acct.borrow<&FreshDealer.Dealer>(from: FreshDealer.DealerStoragePath)
            ?? panic("Could not borrow a reference to the Dealer resource")
    }

    execute {
        // Borrow a reference to the specified set
        let setRef = self.adminRef.borrowSet(setId: setId)

        // Mint multiple tokens
        let collection <- setRef.batchMintCollectible(artId: artId, quantity: quantity)

        // Put tokens collection in the gallery
        self.dealerRef.addArtwork(galleryId: galleryId, artId: artId, collection: <- collection)
    }
}