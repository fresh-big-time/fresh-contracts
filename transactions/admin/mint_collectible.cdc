import FreshArtNFT from "../../contracts/FreshArtNFT.cdc"

// This transaction is what an admin would use to mint a single new edition
// and deposit it in a user's collection

// Parameters:
//
// setId: the ID of a set containing the target play
// artId: the ID of a art to mint from
// recipientAddress: the Flow address of the account receiving the collectible

transaction(setId: UInt32, artId: UInt32, recipientAddress: Address) {
    let adminRef: &FreshArtNFT.Admin

    prepare(acct: AuthAccount) {
        self.adminRef = acct.borrow<&FreshArtNFT.Admin>(from: FreshArtNFT.AdminStoragePath)
            ?? panic("Could not borrow a reference to the Admin resource")
    }

    execute {
        // Borrow a reference to the specified set
        let setRef = self.adminRef.borrowSet(setId: setId)

        // Mint a new NFT
        let token <- setRef.mintCollectible(artId: artId)

        // get the public account object for the recipient
        let recipient = getAccount(recipientAddress)

        // get the Collection reference for the receiver
        let receiverRef = recipient.getCapability(FreshArtNFT.CollectionPublicPath).borrow<&{FreshArtNFT.CollectionPublic}>()
            ?? panic("Cannot borrow a reference to the recipient's collection")

        // deposit the NFT in the receivers collection
        receiverRef.deposit(token: <- token)
    }
}