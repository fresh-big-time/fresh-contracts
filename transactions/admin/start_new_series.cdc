import FreshArtNFT from "../../contracts/FreshArtNFT.cdc"

// This transaction is for an Admin to start a new Fresh Art series

transaction {
    let adminRef: &FreshArtNFT.Admin
    let currentSeries: UInt32

    prepare(acct: AuthAccount) {
        self.adminRef = acct.borrow<&FreshArtNFT.Admin>(from: FreshArtNFT.AdminStoragePath)
            ?? panic("Could not borrow a reference to the Admin resource")
        self.currentSeries = FreshArtNFT.currentSeries
    }
    execute {        
        // Increment the series number
        self.adminRef.startNewSeries()
    }
    post {    
        FreshArtNFT.currentSeries == self.currentSeries + 1 as UInt32: "new series not started"
    }
}
 