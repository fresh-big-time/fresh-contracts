import FreshArtNFT from "../../contracts/FreshArtNFT.cdc"

transaction(setId: UInt32) {
    let adminRef: &FreshArtNFT.Admin

    prepare(acct: AuthAccount) {
        self.adminRef = acct.borrow<&FreshArtNFT.Admin>(from: FreshArtNFT.AdminStoragePath)
            ?? panic("Could not borrow a reference to the Admin resource")
    }
    execute {
        let setRef = self.adminRef.borrowSet(setId: setId)
        setRef.retireAll()
    }
}