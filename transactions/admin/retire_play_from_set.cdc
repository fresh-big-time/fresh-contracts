import FreshArtNFT from "../../contracts/FreshArtNFT.cdc"

// Retired art can no longer mint collectibles

// Parameters:
// 
// setId: the ID of the set in which a play is to be retired
// artId: the ID of the art to be retired

transaction(setId: UInt32, artId: UInt32) {
    let adminRef: &FreshArtNFT.Admin

    prepare(acct: AuthAccount) {
        self.adminRef = acct.borrow<&FreshArtNFT.Admin>(from: FreshArtNFT.AdminStoragePath)
            ?? panic("Could not borrow a reference to the Admin resource")
    }
    execute {
        let setRef = self.adminRef.borrowSet(setId: setId)
        setRef.retireArt(artId: artId)
    }
    post {        
        self.adminRef.borrowSet(setId: setId).retired[artId]!: "art is not retired"
    }
}