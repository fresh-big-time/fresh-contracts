import FungibleToken from "../../contracts/standard/FungibleToken.cdc"
import FreshArtNFT from "../../contracts/FreshArtNFT.cdc"

// Creates new artwork and stores in Contract.
// Still needs to be added to a set.

transaction(artistAddress: Address, mediaType: String?, dataType: String?, data1: String?, data2: String?, metadata: {String: String}) {
    let adminRef: &FreshArtNFT.Admin
    let artistWallet: Capability<&{FungibleToken.Receiver}>
    let currentArtId: UInt32

    prepare(acct: AuthAccount) {
        self.currentArtId = FreshArtNFT.nextArtId;
        self.adminRef = acct.borrow<&FreshArtNFT.Admin>(from: FreshArtNFT.AdminStoragePath) ?? panic("No admin resource in storage")
        let artistAccount = getAccount(artistAddress)
        self.artistWallet = artistAccount.getCapability<&{FungibleToken.Receiver}>(/public/flowTokenReceiver)
    }

    execute {
        let royalties = {
            "artist": FreshArtNFT.Royalty(wallet: self.artistWallet, cut: 0.025)
        }
        self.adminRef.createArt(
            mediaType: mediaType,
            dataType: dataType,
            data1: data1,
            data2: data2,
            royalties: royalties,
            metadata: metadata
        )
    }

    post {        
        FreshArtNFT.getArtwork(artId: self.currentArtId) != nil: "artId doesn't exist"
    }
}