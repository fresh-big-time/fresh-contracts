import FreshArtNFT from "../../contracts/FreshArtNFT.cdc"

// This transaction locks a set so that new plays can no longer be added to it

// Parameters:
//
// setId: the ID of the set to be locked

transaction(setId: UInt32) {

    // local variable for the admin resource
    let adminRef: &FreshArtNFT.Admin

    prepare(acct: AuthAccount) {
        // borrow a reference to the admin resource
        self.adminRef = acct.borrow<&FreshArtNFT.Admin>(from: FreshArtNFT.AdminStoragePath)
            ?? panic("No admin resource in storage")
    }

    execute {
        // borrow a reference to the Set
        let setRef = self.adminRef.borrowSet(setId: setId)

        // lock the set permanently
        setRef.lock()
    }

    post {        
        FreshArtNFT.isSetLocked(setId: setId)!: "Set did not lock"
    }
}