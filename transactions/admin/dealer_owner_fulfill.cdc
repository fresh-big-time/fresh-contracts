import FreshDealer from "../../contracts/FreshDealer.cdc"

// Allows Dealer owner to fulfill any Purchases in Queue
// If fulfillment fails, the process will try to resolve with reimbursement

transaction(galleryId: UInt32) {
    let dealerRef: &FreshDealer.Dealer

    prepare(acct: AuthAccount) {
        self.dealerRef = acct.borrow<&FreshDealer.Dealer>(from: FreshDealer.DealerStoragePath)
            ?? panic("Could not borrow a reference to the Dealer resource")
    }

    execute {
        self.dealerRef.fulfillQueue()
    }
}