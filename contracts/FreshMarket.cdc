/*
 *    _______   _______     _______        _  ,   ____  ____
 *     | | \|    | | \ '.    | | \|     .|` '\|    | |  | |
 *     | |  '    | |  | |    | |  '    | |    |    | |  | |
 *     | |  ,    | |  | |    | |  ,    |  \        | |  | |
 *     | |-<|    | |-< <     | |-<|     \   '.     | |--| |
 *     | |  '    | |  | |    | |  '       '.  |    | |  | |
 *     | |       | |  | |,   | |  ,    |\   | /    | |  | |
 *    _|_|_     _|_|_  \_/  _|_|_/|    |'\_.|/    _|_|__|_|_
 *
 *    FRESHBIGTIME.COM
 *    Created by Tim Davidson, May 2021
 *
 *    Fresh Market
 *
 *    Simple Asker secondary market with purchase functionality for FreshArtNFT.
 *    Based on TopShot Market and a simpler take on my Asker token idea.
 */

//import NonFungibleToken from "./standard/NonFungibleToken.cdc"
import FungibleToken from "./standard/FungibleToken.cdc"
import FreshArtNFT from "./FreshArtNFT.cdc"

pub contract FreshMarket {

  // Events
  pub event Listed(id: UInt64, price: UFix64, seller: Address?)
  pub event Withdrawn(id: UInt64, owner: Address?)
  // Note, FreshArtNFT.Sale handles a successfuly sale event.

  // Named Paths
  pub let MarketStoragePath: StoragePath
  pub let MarketPublicPath: PublicPath

  // ----------------------------------------------------------------
  // Resources Interfaces

  pub resource interface Public {
    pub fun purchase(id: UInt64, paymentTokens: @FungibleToken.Vault): @FreshArtNFT.NFT {
      post {
        result.id == id: "The ID of the withdrawn token must be the same as the requested ID"
      }
    }
    pub fun getPrice(id: UInt64): UFix64?
    pub fun getIds(): [UInt64]
    pub fun borrowCollectible(id: UInt64): &FreshArtNFT.NFT? {
      post {
        (result == nil) || (result?.id == id):
          "Cannot borrow FreshArtNFT reference: The ID of the returned reference is incorrect"
      }
    }
  }

  // ----------------------------------------------------------------
  // Resources
  
  pub resource Collection: Public {
    access(self) var ownerCollection: Capability<&FreshArtNFT.Collection>
    access(self) var paymentReceiver: Capability<&{FungibleToken.Receiver}>
    access(self) var prices: {UInt64: UFix64}

    init(ownerCollection: Capability<&FreshArtNFT.Collection>, paymentReceiver: Capability<&{FungibleToken.Receiver}>) {
      pre {
        ownerCollection.borrow() != nil: "Collection Capability is invalid!"
        paymentReceiver.borrow() != nil: "Payment Receiver Capability is invalid!"
      }

      self.ownerCollection = ownerCollection
      self.paymentReceiver = paymentReceiver
      self.prices = {}
    }

    // Public Functions

    pub fun purchase(id: UInt64, paymentTokens: @FungibleToken.Vault): @FreshArtNFT.NFT {
      pre {
        self.ownerCollection.borrow()!.borrowCollectible(id: id) != nil && self.prices[id] != nil:
          "No token matching this id for sale!"
        paymentTokens.balance == (self.prices[id] ?? (0.0 as UFix64)): "Payment tokens must equal price!"
      }
      let collectionRef = self.ownerCollection.borrow() ?? panic("Failed to borrow reference to Owner's Collection")
      return <- collectionRef.withdrawWithSale(withdrawID: id, paymentTokens: <- paymentTokens, paymentReceiver: self.paymentReceiver)
    }

    pub fun getPrice(id: UInt64): UFix64? {
      return self.prices[id]
    }

    pub fun getIds(): [UInt64] {
      return self.prices.keys
    }

    pub fun borrowCollectible(id: UInt64): &FreshArtNFT.NFT? {
      if self.prices[id] != nil {
        return self.ownerCollection.borrow()!.borrowCollectible(id: id)
      }
      return nil
    }

    // Owner Functions

    pub fun listSale(id: UInt64, price: UFix64) {
      pre {
        self.ownerCollection.borrow()!.borrowCollectible(id: id) != nil: "Collectible not found in owner's collection."
      }
      self.prices[id] = price
      emit Listed(id: id, price: price, seller: self.owner?.address)
    }

    pub fun cancelSale(id: UInt64) {
      pre {
        self.prices[id] != nil: "Token with the specified id is not already for sale."
      }
      self.prices.remove(key: id)
      self.prices[id] = nil
      emit Withdrawn(id: id, owner: self.owner?.address)
    }

    pub fun changePaymentReceiver(_ paymentReceiver: Capability<&{FungibleToken.Receiver}>) {
      pre {
        paymentReceiver.borrow() != nil: "Payment Receiver Capability is invalid!" 
      }
      self.paymentReceiver = paymentReceiver
    }
    
  }

  // ----------------------------------------------------------------
  // Contract Functions

  pub fun createMarketCollection(ownerCollection: Capability<&FreshArtNFT.Collection>,
                                 paymentReceiver: Capability<&{FungibleToken.Receiver}>): @Collection {
    return <- create Collection(ownerCollection: ownerCollection, paymentReceiver: paymentReceiver)
    
    // Up to function caller to store and link
    //self.account.save(<- collection, to: self.MarketStoragePath)
    //self.account.link<&Collection{Public}>(self.MarketPublicPath, target: self.MarketStoragePath)
  }

  init() {
    self.MarketStoragePath = /storage/FreshMarketCollection
    self.MarketPublicPath = /public/FreshMarketCollection
  }

}
