/*
 *    _______   _______     _______        _  ,   ____  ____
 *     | | \|    | | \ '.    | | \|     .|` '\|    | |  | |
 *     | |  '    | |  | |    | |  '    | |    |    | |  | |
 *     | |  ,    | |  | |    | |  ,    |  \        | |  | |
 *     | |-<|    | |-< <     | |-<|     \   '.     | |--| |
 *     | |  '    | |  | |    | |  '       '.  |    | |  | |
 *     | |       | |  | |,   | |  ,    |\   | /    | |  | |
 *    _|_|_     _|_|_  \_/  _|_|_/|    |'\_.|/    _|_|__|_|_
 *
 *    FRESHBIGTIME.COM
 *    Created by Tim Davidson, July 2021
 *
 *    Fresh Dealer
 *
 *    Primary market platform for minted NFTs. Delivers a random NFT from a Gallery of Artwork.
 *    A gallery contains multiple artwork sold randomly together, artwork minted to multipel serials.
 *    Users can spend more, at a curving price, to exclude artworks from the random selection,
 *    spending maximum high price to select specific Artwork. A NFT serial is delivered randomly
 *    from the selected artwork in the Gallery. All information on which serials are still
 *    available should be given to user when buying.
 *
 *    Reliable randomization is achieved by splitting into multiple transactions:
 *      - Purchase:   User gives Art Ids with payment, NFT Receiver, and Reimburse FT Receiver.
 *                    Purchase is queued.
 *      - Fulfill:    Admin then fulfills from queue (in order), randomly selects NFT and make sale.
 *      - Reimburse:  If Fulfill fails for any reason, then Admin reimburses payment vault.
 *
 *    Example:
 *
 *    A gallery was 10 different artworks, each with 30 serials. A table shows which serials are
 *    still available. Some artworks may have more left than others, so a percentage chance is
 *    disdplayed for each Artwork to. Price range is $5 to $50. Initially, all 10 artworks are
 *    checked and price is $5. Some artworks may be less desirable, or no longer have low serials
 *    available. The user can uncheck artwork, increasing the price at a curve. Selecting only one
 *    artwork will be maximum price, $50. There is still always a chance the $5 purchase results in
 *    the lowest serial of the more desirable artwork.
 *
 *    The user selects from 4 different artworks, at say $20, and completes a Purchase transaction.
 *    The system triggers the Admin Account to fulfill the Queued Purchase. The fulfill transaction
 *    randomly selects 1 NFT from all available NFTs for the 4 selected artworks. The payment is
 *    completed and the NFT is delivered to the buyer's Collection.
 */


import NonFungibleToken from "./standard/NonFungibleToken.cdc"
import FungibleToken from "./standard/FungibleToken.cdc"
import FreshArtNFT from "./FreshArtNFT.cdc"

pub contract FreshDealer {

  // Events
  pub event PurchasePaid(uid: UInt64, artIds: [UInt32], buyer: Address?)
  pub event PurchaseFulfilled(uid: UInt64, nftId: UInt64, buyer: Address?)
  pub event PurchaseReimbursed(uid: UInt64, buyer: Address?)
  pub event PurchasePaymentIsNil(uid: UInt64) // This shouldn't happen

  // Note, FreshArtNFT.Sale also emits a successfuly sale event.

  // Named Paths
  pub let DealerStoragePath: StoragePath
  pub let DealerPublicPath: PublicPath

  // ----------------------------------------------------------------
  // Variables

  pub var nextGalleryId: UInt32

  // ----------------------------------------------------------------
  // Structs

  // NFT artId and nftId, used in randomization
  pub struct ArtIdNftId {
    pub let artId: UInt32
    pub let nftId: UInt64
    init(_ artId: UInt32, _ nftId: UInt64) {
      self.artId = artId
      self.nftId = nftId
    }
  }

  // Used by scripts to serialize Data for the Gallery
  pub struct GalleryView {
    pub let galleryId: UInt32
    pub let isOpen: Bool
    pub let prices: [UFix64]
    // Dictionary indexed by artId, containing arrays of available serials
    // (instead of collection of NFTViews, which was too much info)
    pub let serials: {UInt32: [UInt32]}
    init(gallery: &Gallery) {
      self.galleryId = gallery.galleryId
      self.isOpen = gallery.isOpen
      self.prices = gallery.prices
      self.serials = {}
      let artIds = gallery.collections.keys
      for id in artIds {
        let col = gallery.borrowCollection(artId: id)
        self.serials[id] = col.getSerials()
      }
    }
  }


  // ----------------------------------------------------------------
  // Resources Interfaces

  pub resource interface Public {
    pub fun getPurchaseQueueLength(): Int
    pub fun getGalleryIds(): [UInt32]
    pub fun borrowGallery(galleryId: UInt32): &Gallery
    pub fun getGalleryViewFor(galleryId: UInt32): GalleryView
    pub fun getIsOpenFor(galleryId: UInt32): Bool
    pub fun getPricesFor(galleryId: UInt32): [UFix64]
    pub fun getPriceFor(galleryId: UInt32, count: Int): UFix64
    pub fun getArtIdsFor(galleryId: UInt32): [UInt32]
    pub fun purchase(galleryId: UInt32, artIds: [UInt32], 
                     receiver: Capability<&{NonFungibleToken.Receiver}>,
                     paymentTokens: @FungibleToken.Vault,
                     reimburseReceiver: Capability<&{FungibleToken.Receiver}>): UInt64
  }

  // ----------------------------------------------------------------
  // Resources

  // A set of art for sale.
  pub resource Gallery {
    pub let galleryId: UInt32
    access(contract) var lowPrice: UFix64
    access(contract) var highPrice: UFix64
    access(contract) var isOpen: Bool

    // Prices from single (highPrice) to total collection length (lowPrice) on curve
    access(contract) var prices: [UFix64]

    // Receives payments for Artwork purchased from gallery
    access(contract) var paymentReceiver: Capability<&{FungibleToken.Receiver}>

    // NFTs in the gallery. Indexed by FreshArtNFT.Art.artId 
    access(contract) var collections: @{UInt32: FreshArtNFT.Collection}

    init(paymentReceiver: Capability<&{FungibleToken.Receiver}>, lowPrice: UFix64, highPrice: UFix64) {
      pre {
        paymentReceiver.borrow() != nil: "Payment Receiver Capability failed!"
      }
      self.galleryId = FreshDealer.nextGalleryId
      self.paymentReceiver = paymentReceiver
      self.lowPrice = lowPrice
      self.highPrice = highPrice
      self.isOpen = false
      self.collections <- {}
      self.prices = []

      FreshDealer.nextGalleryId = FreshDealer.nextGalleryId + (1 as UInt32)
    }

    access(contract) fun borrowCollection(artId: UInt32): &FreshArtNFT.Collection {
      pre { self.collections[artId] != nil: "Cannot borrow Collection: doesn't exist." }
      return &self.collections[artId] as &FreshArtNFT.Collection  
    }

    access(contract) fun setPrice(low: UFix64, high: UFix64) {
      self.lowPrice = low
      self.highPrice = high
    }

    access(contract) fun setIsOpen(_ isOpen: Bool) {
      self.isOpen = isOpen
    }

    access(contract) fun calculatePrices() {
        self.prices = FreshDealer.calculatePrices(count: Fix64(self.collections.length),
                          lowPrice: Fix64(self.lowPrice), highPrice: Fix64(self.highPrice), curve: 0.5)
    }

    // Withdraw an NFT randomly from artIds without payment. Owner can redeem gift codes.
    access(contract) fun randomWithdraw(artIds: [UInt32]): @NonFungibleToken.NFT {
      let random: ArtIdNftId = self.randomNftId(from: artIds) ?? panic("No random NFT id determined.") 
      let collectionRef = &self.collections[random.artId] as &FreshArtNFT.Collection
      return <- collectionRef.withdraw(withdrawID: random.nftId)
    }

    // Fulfull Step 1: Selects random artId and nftId from all NFTs available in gallery
    // If this process failed, return nil so we can complete transaction with reimbursement.
    access(contract) fun randomNftId(from artIds: [UInt32]): ArtIdNftId? {
      let deck: [ArtIdNftId] = []
      for artId in artIds {
        let nftIds = self.collections[artId]?.getIDs()
        if let nftIds = nftIds {
          for nftId in nftIds {
            deck.append(ArtIdNftId(artId, nftId))
          }
        } else {
          log("Random NFT Failed: Art collection not found.")
          return nil
        }
      }
      let len = UInt64(deck.length)
      if len == 0 {
        log("Random NFT Failed: Found no NFTs with matching art ids in gallery.")
          return nil
      }
      return deck[unsafeRandom() % len]
    }

    // Fulfull Step 2: complete withdraw and sale with successfully selected random NFT
    access(contract) fun withdrawWithSale(random: ArtIdNftId, paymentTokens: @FungibleToken.Vault): @FreshArtNFT.NFT {
      let collectionRef = &self.collections[random.artId] as &FreshArtNFT.Collection
      return <- collectionRef.withdrawWithSale(withdrawID: random.nftId,
                                              paymentTokens: <- paymentTokens,
                                              paymentReceiver: self.paymentReceiver)
    }

    destroy() {
      destroy self.collections
    }
  }


  // Outstanding purchases to fulfill, or failing that reimburse
  pub resource Purchase {
    pub let uniqueId: UInt64
    pub let galleryId: UInt32
    pub let artIds: [UInt32]
    pub let receiver: Capability<&{NonFungibleToken.Receiver}>
    pub(set) var paymentTokens: @FungibleToken.Vault?
    pub let reimburseReceiver:Capability<&{FungibleToken.Receiver}> 

    init(galleryId: UInt32, artIds: [UInt32], 
         receiver: Capability<&{NonFungibleToken.Receiver}>,
         paymentTokens: @FungibleToken.Vault,
         reimburseReceiver: Capability<&{FungibleToken.Receiver}>) {
      self.uniqueId = unsafeRandom()
      self.galleryId = galleryId
      self.artIds = artIds
      self.receiver = receiver
      self.paymentTokens <- paymentTokens
      self.reimburseReceiver = reimburseReceiver
    }
    destroy() {
      destroy self.paymentTokens
    }
  }


  pub resource Dealer: Public {
    pub var purchaseQueue: @[Purchase]
    access(self) var galleries: @{UInt32: Gallery}

    init () {
      self.purchaseQueue <- []
      self.galleries <- {}
    }

    // Public Functions

    pub fun getPurchaseQueueLength(): Int {
      return self.purchaseQueue.length
    }

    pub fun getGalleryIds(): [UInt32] {
      return self.galleries.keys
    }

    pub fun borrowGallery(galleryId: UInt32): &Gallery {
      pre { self.galleries[galleryId] != nil: "Cannot borrow Gallery: doesn't exist." }
      return &self.galleries[galleryId] as &Gallery
    }

    pub fun getGalleryViewFor(galleryId: UInt32): GalleryView {
      let gallery = self.borrowGallery(galleryId: galleryId)
      return GalleryView(gallery: gallery)
    }

    pub fun getIsOpenFor(galleryId: UInt32): Bool {
      let gallery = self.borrowGallery(galleryId: galleryId)
      return gallery.isOpen
    }

    pub fun getPricesFor(galleryId: UInt32): [UFix64] {
      let gallery = self.borrowGallery(galleryId: galleryId)
      return gallery.prices
    }

    pub fun getPriceFor(galleryId: UInt32, count: Int): UFix64 {
      let gallery = self.borrowGallery(galleryId: galleryId)
      if gallery.prices.length >= count {
        panic("Count is higher than available prices.")
      }
      return gallery.prices[count]
    }

    pub fun getArtIdsFor(galleryId: UInt32): [UInt32] {
      let gallery = self.borrowGallery(galleryId: galleryId)
      return gallery.collections.keys
    }

    pub fun getAvailableSerialsFor(galleryId: UInt32, artId: UInt32): [UInt32] {
      let gallery = self.borrowGallery(galleryId: galleryId)
      let collection = gallery.borrowCollection(artId: artId)
      return collection.getSerials()
    }

    pub fun purchase(galleryId: UInt32, artIds: [UInt32], 
                     receiver: Capability<&{NonFungibleToken.Receiver}>,
                     paymentTokens: @FungibleToken.Vault,
                     reimburseReceiver: Capability<&{FungibleToken.Receiver}>): UInt64 {
      let gallery = self.borrowGallery(galleryId: galleryId)
      let count = artIds.length

      // TO DO: more verification that everything is working, NFTs exist

      if !gallery.isOpen { panic("Gallery is closed!") }

      // Verify price
      if (paymentTokens.balance != gallery.prices[count]) {
        panic("Payment tokens must equal price for selected count exactly!")
      }

      // Verify buyer's receivers
      let receiverRef = receiver.borrow() ?? panic("NFT Receiver capability failed.")
      reimburseReceiver.borrow() ?? panic("Reimbursement FT Receiver capability failed.")

      let purchase <- create Purchase(galleryId: galleryId, artIds: artIds, 
         receiver: receiver,
         paymentTokens: <- paymentTokens,
         reimburseReceiver: reimburseReceiver)
      let uid = purchase.uniqueId
      
      // Add purchase to end of Queue
      self.purchaseQueue.append(<- purchase)

      emit PurchasePaid(uid: uid, artIds: artIds, buyer: receiverRef.owner?.address)
      return uid
    }


    // Owner Functions

    // Resolves all purchases in Queue, starting with first
    // Tries to resolve any fails by reimbursing the payment
    pub fun fulfillQueue() {
      while self.purchaseQueue.length > 0 {
        let purchase <- self.purchaseQueue.removeFirst()

        if let receiverRef = purchase.receiver.borrow() {
          let galleryRef = &self.galleries[purchase.galleryId] as &Gallery

          if let random: ArtIdNftId = galleryRef.randomNftId(from: purchase.artIds) {
            if let paymentTokens <- purchase.paymentTokens <- nil {
              let token <- galleryRef.withdrawWithSale(random: random, paymentTokens: <- paymentTokens)

              let nftId: UInt64 = token.id
              receiverRef.deposit(token: <- token)

              let uid = purchase.uniqueId
              destroy purchase
              emit PurchaseFulfilled(uid: uid, nftId: nftId, buyer: receiverRef.owner?.address)

            } else {
              // Payment Tokens were nil. This shouldn't happen, and wouldn't be able to reimburse.
              // So if payment tokens are nil then all we can do is destroy the purchase.
              let uid = purchase.uniqueId
              destroy purchase
              emit PurchasePaymentIsNil(uid: uid)
            }
          } else {
            // Failed to find random NFT, ie. art ids not found.
            self.reimburse(purchase: <- purchase)
          }
        } else {
          // NFT Receiver capability failed, try to reimburse.
          self.reimburse(purchase: <- purchase)
        }
      }
    }

    // Resolve Purchase by returning vault
    pub fun reimburse(purchase: @Purchase) {
      let receiverRef = purchase.reimburseReceiver.borrow() ?? panic("Reimbursement FT Receiver capability failed.")
      if let vault <- purchase.paymentTokens <- nil {
        receiverRef.deposit(from: <-vault)
      }
      let uid = purchase.uniqueId
      destroy purchase
      emit PurchaseReimbursed(uid: uid, buyer: receiverRef.owner?.address)
    }

    // For monitoring outstanding Purches in Queue.
    pub fun getPurchaseQueueUids(): [UInt64] {
      let uids: [UInt64] = []
      let len = self.purchaseQueue.length
      var i = 0
      while (i < len) {
        let p = &self.purchaseQueue[i] as &Purchase
        uids.append(p.uniqueId)
        i = i + 1
      }
      return uids
    }

    // A way to clean out broken Purchases that get stuck in queue. Returns payment if can.
    pub fun destroyPurchaseInQueue(uid: UInt64): @FungibleToken.Vault? {
      let purchase <- self.purchaseQueue.remove(at: self.indexFor(uid: uid))
      let paymentTokens <- purchase.paymentTokens <- nil
      destroy purchase
      return <- paymentTokens
    }

    // Withdraws random NFT of artIds without Purchase or Queue, for redeeming codes
    pub fun randomWithdraw(galleryId: UInt32, artIds: [UInt32]): @NonFungibleToken.NFT {
      let gallery = self.borrowGallery(galleryId: galleryId)
      return <- gallery.randomWithdraw(artIds: artIds)
    }

    // Finds Queue index for unique id
    access(self) fun indexFor(uid: UInt64): Int {
      let len = self.purchaseQueue.length
      var i = 0
      while (i < len) {
        let p = &self.purchaseQueue[i] as &Purchase
        if (p.uniqueId == uid) {
          return i
        }
        i = i + 1
      }
      panic("Unique Id not found in Purchase Queue.")
    }

    // Creates a new gallery, with empty collections, and returns its id.
    pub fun createGallery(paymentReceiver: Capability<&{FungibleToken.Receiver}>, lowPrice: UFix64, highPrice: UFix64): UInt32 {
      let newGallery <- create Gallery(paymentReceiver: paymentReceiver, lowPrice: lowPrice, highPrice: highPrice)
      let id: UInt32 = newGallery.galleryId
      let trash <- self.galleries[id] <- newGallery
      destroy trash
      return id
    }

    // Destroys an empty gallery
    pub fun destroyGallery(galleryId: UInt32) {
      pre { self.galleries[galleryId] != nil: "Gallery id doesn't exist." }
      let galleryRef = &self.galleries[galleryId] as &Gallery
      if (galleryRef.collections.length != 0) {
        panic("Gallery is not empty. Remove all artwork first.")
      }
      destroy self.galleries.remove(key: galleryId)
    }

    // Add artwork and the collection NFTs to a closed gallery
    pub fun addArtwork(galleryId: UInt32, artId: UInt32, collection: @FreshArtNFT.Collection) {
      pre { self.galleries[galleryId] != nil: "Gallery id doesn't exist." }
      let galleryRef = &self.galleries[galleryId] as &Gallery
      if galleryRef.isOpen {
        panic("Cannot add art to an open gallery.") 
      }
      if galleryRef.collections[artId] != nil {
        panic("Art Id already in gallery.") 
      }
      let trash <- galleryRef.collections[artId] <- collection
      destroy trash
    }

    // Remove artwork from gallery, returning collection
    pub fun removeArtwork(galleryId: UInt32, artId: UInt32): @FreshArtNFT.Collection {
      pre { self.galleries[galleryId] != nil: "Gallery id doesn't exist." }
      let galleryRef = &self.galleries[galleryId] as &Gallery
      if galleryRef.isOpen {
        panic("Cannot remove art from an open gallery.") 
      }
      let collection <- galleryRef.collections.remove(key: artId) ?? panic("Art id not found in gallery")
      return <- collection
    }

    // Assigns price range for a closed gallery. Prices calculated at openning.
    pub fun setPrice(galleryId: UInt32, low: UFix64, high: UFix64) {
      pre { self.galleries[galleryId] != nil: "Gallery id doesn't exist." }
      let galleryRef = &self.galleries[galleryId] as &Gallery
      if galleryRef.isOpen {
        panic("Cannot set prices to an open gallery.") 
      }
      galleryRef.setPrice(low: low, high: high)
    }

    // Opens a gallery for sale, assigning price amounts based on the artwork count
    pub fun setOpenForSale(galleryId: UInt32, isOpen: Bool) {
      pre { self.galleries[galleryId] != nil: "Gallery id doesn't exist." }
      let galleryRef = &self.galleries[galleryId] as &Gallery
      if isOpen {
        let count = galleryRef.collections.length
        if count == 0 { panic("Gallery is empty.") }
        galleryRef.calculatePrices()
      }
      galleryRef.setIsOpen(isOpen)
    }

    destroy() {
      destroy self.purchaseQueue
      destroy self.galleries
    }
  }

  // ----------------------------------------------------------------
  // Contract Functions


  pub fun calculatePrices(count: Fix64, lowPrice: Fix64, highPrice: Fix64, curve a: Fix64): [UFix64] {
    let prices: [UFix64] = []
    let b = (highPrice - lowPrice + (a * count * count)) / (1.0 - count)
    let c = highPrice - b

    var x = 1.0 as Fix64
    while x <= count {
      let y = self.floor( (a*x*x) + (b*x) + c )
      prices.append(y)
      x = x + 1.0
    }
    return prices
  }

  pub fun floor(_ value: Fix64): UFix64 {
    return UFix64(Int(value))
  }

  init() {
    // Set our named paths
    self.DealerStoragePath = /storage/FreshDealerCollection
    self.DealerPublicPath = /public/FreshDealerCollection

    // Initialize variables stored in the Contract
    self.nextGalleryId = 1

    // If Dealer doesn't already exist here.
    if !self.account.getCapability<&{Public}>(self.DealerPublicPath).check() {
      // Create a Dealer and store here
      let dealer <- create Dealer()
      self.account.save(<-dealer, to: self.DealerStoragePath)
      // Create a public capability for the Dealer
      self.account.link<&{Public}>(self.DealerPublicPath, target: self.DealerStoragePath)
    }

  }

}
