/*
 *    _______   _______     _______        _  ,   ____  ____
 *     | | \|    | | \ '.    | | \|     .|` '\|    | |  | |
 *     | |  '    | |  | |    | |  '    | |    |    | |  | |
 *     | |  ,    | |  | |    | |  ,    |  \        | |  | |
 *     | |-<|    | |-< <     | |-<|     \   '.     | |--| |
 *     | |  '    | |  | |    | |  '       '.  |    | |  | |
 *     | |       | |  | |,   | |  ,    |\   | /    | |  | |
 *    _|_|_     _|_|_  \_/  _|_|_/|    |'\_.|/    _|_|__|_|_
 *
 *    FRESHBIGTIME.COM
 *    Created by Tim Davidson, April 2021
 *
 *    Fresh Art NFT
 *
 *    Primary NFT for FRESH Big Time Art.
 *    Conforms to NonFungibleToken standard.
 *
 *    Ended up pivoting more towards Top Shot and Evolution, with sets of art.
 *    Each NFT is a particular edition of art.
 *    The primary difference is Fresh Big Time will include all asset data
 *    in the NFT, at least for SVG based art, and dynamic data.
 *
 *    The NFT contains it's own Royalty and completeSale function which
 *    updates dynamic data stored on the NFT itself.
 */

import NonFungibleToken from "./standard/NonFungibleToken.cdc"
import FungibleToken from "./standard/FungibleToken.cdc"

pub contract FreshArtNFT: NonFungibleToken {

  // ----------------------------------------------------------------
  // Events
  //
  // Events based on NBA Top Shot and Evolution.
  // Rest of this contract refers to Item as "Art" for clarity, but kept their event naming.

  pub event ContractInitialized()

  // Events for Set and COllectible actions
  pub event NewSeriesStarted(newCurrentSeries: UInt32)
  pub event SetCreated(setId: UInt32, series: UInt32)
  pub event ItemCreated(id: UInt32, metadata: {String:String})
  pub event ItemAddedToSet(setId: UInt32, itemId: UInt32)
  pub event ItemRetiredFromSet(setId: UInt32, itemId: UInt32, minted: UInt32)
  pub event SetLocked(setId: UInt32)
  pub event CollectibleMinted(id: UInt64, itemId: UInt32, setId: UInt32, serialNumber: UInt32)
  pub event CollectibleDestroyed(id: UInt64)

  // Events for Collection-related actions
  pub event Sale(id: UInt64, serial: UInt32, price: UFix64, saleCount: UInt16)
  pub event Withdraw(id: UInt64, from: Address?)
  pub event Deposit(id: UInt64, to: Address?)


  // ----------------------------------------------------------------
  // Named Paths
  pub let CollectionStoragePath: StoragePath
  pub let CollectionPublicPath: PublicPath
  pub let AdminStoragePath: StoragePath

  // ----------------------------------------------------------------
  // Variables
  pub var currentSeries: UInt32

  access(self) var artwork: {UInt32: Art}
  access(self) var setDatas: {UInt32: SetData}
  access(self) var sets: @{UInt32: Set}

  pub var nextArtId: UInt32
  pub var nextSetId: UInt32
  pub var totalSupply: UInt64

  // ----------------------------------------------------------------
  // Structs

  // Data on each set, stored in this contract.
  pub struct SetData {
    pub let setId: UInt32
    pub let name: String
    pub let description: String?
    pub let series: UInt32
    init(name: String, description: String?) {
      pre { name.length > 0: "New Set name cannot be empty" }
      self.setId = FreshArtNFT.nextSetId
      self.name = name
      self.description = description
      self.series = FreshArtNFT.currentSeries
      FreshArtNFT.nextSetId = FreshArtNFT.nextSetId + (1 as UInt32)
      emit SetCreated(setId: self.setId, series: self.series)
    }
  }
  
  // Following structs are stored in each NFT

  // Royalty fees given on each sale
  pub struct Royalty {
    pub let receiver:Capability<&{FungibleToken.Receiver}> 
    pub let cut: UFix64

    init(receiver:Capability<&{FungibleToken.Receiver}>, cut: UFix64 ){
      self.receiver = receiver
      self.cut = cut
    }
  }

  // Stored the primary data for art.
  // A copy of which is stored in every NFT
  pub struct Art {
    pub let artId: UInt32
    pub let mediaType: String?    // ie. "image/svg+xml", "image/jpeg", "image/png"
    pub let dataType: String?     // ie. "base64", "svg", "svg+js"

    // Data is seperated into two parts so that dynamic <script> can be costlessly
    // inserted within the asset. If no dynamic data is used, can just use data1
    access(contract) let data1: String?
    access(contract) let data2: String?

    // Stores name: royalty share
    pub let royalties: {String: Royalty}

    // Other Art metadata. Can be anything, ie:
    // title, artist, set, dimensions, width, height, medium, surface, description
    pub var metadata: {String: String}

    init(mediaType: String?, dataType: String?, data1: String?, data2: String?,
         royalties: {String: Royalty}, metadata: {String: String}) {
      self.mediaType = mediaType
      self.dataType = dataType
      self.data1 = data1
      self.data2 = data2
      self.royalties = royalties
      self.metadata = metadata
      self.artId = FreshArtNFT.nextArtId
      FreshArtNFT.nextArtId = FreshArtNFT.nextArtId + (1 as UInt32)
      emit ItemCreated(id: self.artId, metadata: metadata)
    }
  }

  // Dynamic data that updates on sales.
  // Usable for procedural effects on the artwork itself!
  pub struct Dynamic {
    pub(set) var lastPrice: UFix64
    pub(set) var penultPrice: UFix64
    pub(set) var saleCount: UInt16
    init(lastPrice: UFix64, penultPrice: UFix64, saleCount: UInt16) {
      self.lastPrice = lastPrice
      self.penultPrice = penultPrice
      self.saleCount = saleCount
    }
  }

  // Used by scripts to serialize NFT data.
  // Flat view with just artId, not all data.
  pub struct NFTView {
    pub let nftId: UInt64
    pub let serialNumber: UInt32
    pub let artId: UInt32
    pub let lastPrice: UFix64
    pub let penultPrice: UFix64
    pub let saleCount: UInt16
    init(nft: &NFT) {
      self.nftId = nft.id
      self.serialNumber = nft.serialNumber
      self.artId = nft.art.artId
      self.lastPrice = nft.dynamic.lastPrice
      self.penultPrice = nft.dynamic.penultPrice
      self.saleCount = nft.dynamic.saleCount
    }
  }

  // Used by scripts to serialize NFT data.
  // Variation with complete Art data
  pub struct NFTViewWithData {
    pub let nftId: UInt64
    pub let serialNumber: UInt32
    pub let art: Art
    pub let dynamic: Dynamic
    init(nft: &NFT) {
      self.nftId = nft.id
      self.serialNumber = nft.serialNumber
      self.art = nft.art
      self.dynamic = nft.dynamic
    }
  }

  // ----------------------------------------------------------------
  // Resources Interfaces

  // This is the interface that users can cast their Art Collection as
  // to allow others to deposit Art into their Collection. It also allows for reading
  // the details of Art in the Collection.
  pub resource interface CollectionPublic {
    pub fun deposit(token: @NonFungibleToken.NFT)
    pub fun batchDeposit(tokens: @NonFungibleToken.Collection)
    pub fun getIDs(): [UInt64]
    pub fun getSerials(): [UInt32]
    pub fun getNFTViews(): [NFTView]
    pub fun getNFTViewsWithData(): [NFTViewWithData]
    pub fun borrowNFT(id: UInt64): &NonFungibleToken.NFT
    pub fun borrowCollectible(id: UInt64): &FreshArtNFT.NFT? {
      post {
        (result == nil) || (result?.id == id):
          "Cannot borrow FreshArtNFT reference: The ID of the returned reference is incorrect"
      }
    }
    pub fun getAddress(): Address?
  }

  // ----------------------------------------------------------------
  // Resources

  // The primary Art resource
  pub resource NFT: NonFungibleToken.INFT {
    pub let id: UInt64            // The id of the NFT
    pub let serialNumber: UInt32  // Unique number to this copy of art

    // Complete art stored with each NFT
    pub let art: Art

    // Dynamic data updated on each withdrawWithSale
    access(contract) var dynamic: Dynamic

    init(serialNumber: UInt32, art: Art, setId: UInt32) {
      self.serialNumber = serialNumber
      self.art = art
      self.dynamic = Dynamic(lastPrice: 0.0, penultPrice: 0.0, saleCount: 0)

      FreshArtNFT.totalSupply = FreshArtNFT.totalSupply + (1 as UInt64)
      self.id = FreshArtNFT.totalSupply

      emit CollectibleMinted(id: self.id, itemId: art.artId, setId: setId, serialNumber: self.serialNumber)
    }

    pub fun data(): String? {
      // if both data exist, concat with dynamic data between!
      if let data2 = self.art.data2 {
        if let data1 = self.art.data1 {
          let dynamic = "<script>s=".concat(self.serialNumber.toString())
                      .concat(";p=").concat(self.dynamic.lastPrice.toString())
                      .concat(";q=").concat(self.dynamic.penultPrice.toString())
                      .concat(";c=").concat(self.dynamic.saleCount.toString())
                      .concat("</script>")
          return data1.concat(dynamic).concat(data2)
        }
      }
      return self.art.data1
    }

    destroy() {
      emit CollectibleDestroyed(id: self.id)
    }
  }

  // Stores the art NFTs, conforming to NFT interaction standards.
  pub resource Collection: CollectionPublic,
            NonFungibleToken.Provider, NonFungibleToken.Receiver, NonFungibleToken.CollectionPublic {
    pub var ownedNFTs: @{UInt64: NonFungibleToken.NFT}

    init () {
      self.ownedNFTs <- {}
    }

    // Completes a sale, handling payment and royalties.
    // Must call this to add to dynamic data.
    pub fun withdrawWithSale(withdrawID: UInt64,
                             paymentTokens: @FungibleToken.Vault,
                             paymentReceiver: Capability<&{FungibleToken.Receiver}>): @FreshArtNFT.NFT {
      let paymentReceiver = paymentReceiver.borrow() ?? panic("Payment Receiver capability failed.")
      let price = paymentTokens.balance
      let nft <- self.ownedNFTs.remove(key: withdrawID) ?? panic("missing NFT")
      let token <- nft as! @FreshArtNFT.NFT

      // Subtract royalty fees
      for key in token.art.royalties.keys {
        let royalty = token.art.royalties[key]!
        let amount = price * royalty.cut
        if let receiver = royalty.receiver.borrow() {
          receiver.deposit(from: <- paymentTokens.withdraw(amount: amount) )
        }
      }

      // Deposit $$ tokens in paymentReceiver
      paymentReceiver.deposit(from: <- paymentTokens)

      // Update dynamic data
      token.dynamic.penultPrice = token.dynamic.lastPrice
      token.dynamic.lastPrice = price
      token.dynamic.saleCount = token.dynamic.saleCount + (1 as UInt16)

      emit Withdraw(id: token.id, from: self.owner?.address)
      emit Sale(id: token.id, serial: token.serialNumber, price: price, saleCount: token.dynamic.saleCount)
      return <-token
    }

    pub fun withdraw(withdrawID: UInt64): @NonFungibleToken.NFT {
      let token <- self.ownedNFTs.remove(key: withdrawID) ?? panic("missing NFT")
      emit Withdraw(id: token.id, from: self.owner?.address)
      return <-token
    }

    pub fun batchWithdraw(ids: [UInt64]): @NonFungibleToken.Collection {
      var batchCollection <- create Collection()
      for id in ids {
        batchCollection.deposit(token: <-self.withdraw(withdrawID: id))
      }
      return <-batchCollection
    }

    pub fun deposit(token: @NonFungibleToken.NFT) {
      let token <- token as! @FreshArtNFT.NFT // Cast to FreshArtNFT
      let id: UInt64 = token.id
      let trash <- self.ownedNFTs[id] <- token
      destroy trash
      emit Deposit(id: id, to: self.owner?.address)
    }

    pub fun batchDeposit(tokens: @NonFungibleToken.Collection) {
      let keys = tokens.getIDs()
      for key in keys {
        self.deposit(token: <-tokens.withdraw(withdrawID: key))
      }
      destroy tokens
    }

    pub fun getIDs(): [UInt64] {
      return self.ownedNFTs.keys
    }

    pub fun getSerials(): [UInt32] {
      let serials: [UInt32] = []
      let ids = self.ownedNFTs.keys
      for id in ids {
        if let nft = self.borrowCollectible(id: id) {
          serials.append(nft.serialNumber)
        }
      }
      return serials
    }

    pub fun getNFTViews(): [NFTView] {
      let views: [NFTView] = []
      let ids = self.ownedNFTs.keys
      for id in ids {
        if let nft = self.borrowCollectible(id: id) {
          views.append(NFTView(nft: nft))
        }
      }
      return views
    }

    pub fun getNFTViewsWithData(): [NFTViewWithData] {
      let views: [NFTViewWithData] = []
      let ids = self.ownedNFTs.keys
      for id in ids {
        if let nft = self.borrowCollectible(id: id) {
          views.append(NFTViewWithData(nft: nft))
        }
      }
      return views
    }

    pub fun borrowNFT(id: UInt64): &NonFungibleToken.NFT {
      return &self.ownedNFTs[id] as &NonFungibleToken.NFT
    }

    pub fun borrowCollectible(id: UInt64): &FreshArtNFT.NFT? {
      if self.ownedNFTs[id] != nil {
        let ref = &self.ownedNFTs[id] as auth &NonFungibleToken.NFT
        return ref as! &FreshArtNFT.NFT
      }
      return nil
    }

    pub fun getAddress(): Address? {
      return self.owner?.address
    }

    destroy() {
      destroy self.ownedNFTs
    }
  }

  // Set allows the Admin to manage the creation of artwork and mint NFTs
  pub resource Set {
    pub let setId: UInt32
    pub var artIds: [UInt32]
    pub var retired: {UInt32: Bool}
    pub var locked: Bool // Locking is final
    pub var numberMintedPerItem: {UInt32: UInt32}
    
    init(name: String, description: String?) {
      self.setId = FreshArtNFT.nextSetId // incremented by SetData struct
      self.artIds = []
      self.retired = {}
      self.locked = false
      self.numberMintedPerItem = {}

      // Create a new SetData for this Set and store it in contract storage
      FreshArtNFT.setDatas[self.setId] = SetData(name: name, description: description)
    }

    // Associates artwork with this Set. A particular art Id can actually be in multiple sets.
    pub fun addArt(artId: UInt32) {
      pre {
        FreshArtNFT.artwork[artId] != nil: "Cannot add the Art to Set: Artwork data doesn't exist."
        !self.locked: "Cannot add the Item to the Set after the set has been locked."
        self.numberMintedPerItem[artId] == nil: "The Item has already beed added to the set."
      }
      self.artIds.append(artId)
      self.retired[artId] = false
      self.numberMintedPerItem[artId] = 0
      emit ItemAddedToSet(setId: self.setId, itemId: artId)
    }

    pub fun addMultipleArt(artIds: [UInt32]) {
      for id in artIds {
        self.addArt(artId: id)
      }
    }

    pub fun retireArt(artId: UInt32) {
      pre { self.retired[artId] != nil: "Cannot retire the Item: Item doesn't exist in this set!" }
      if !self.retired[artId]! {
        self.retired[artId] = true
        emit ItemRetiredFromSet(setId: self.setId, itemId: artId, minted: self.numberMintedPerItem[artId]!)
      }
    }

    pub fun retireAll() {
      for id in self.artIds {
        self.retireArt(artId: id)
      }
    }

    pub fun lock() {
      if !self.locked {
        self.locked = true
        emit SetLocked(setId: self.setId)
      }
    }

    // Create a new NFT and return
    pub fun mintCollectible(artId: UInt32): @FreshArtNFT.NFT {
      pre {
        !self.retired[artId]!: "Cannot mint the collectible, art id has been retired."
      }
      let art = FreshArtNFT.artwork[artId] ?? panic("Cannot mint the collectible, artwork not found.")
      let minted = self.numberMintedPerItem[artId] ?? panic("Cannot mint the collectible, minted count not found.")
      let serial = minted + (1 as UInt32)
      self.numberMintedPerItem[artId] = serial

      let newNFT <- create NFT(serialNumber: serial, art: art, setId: self.setId)
      return <- newNFT
    }

    // Create a collection with editionCount copies of NFT
    pub fun batchMintCollectible(artId: UInt32, quantity: UInt64): @FreshArtNFT.Collection {
      let newCollection <- create Collection()
      var i: UInt64 = 0
      while i < quantity {
        newCollection.deposit(token: <-self.mintCollectible(artId: artId))
        i = i + (1 as UInt64)
      }
      return <-newCollection
    }

  }

  // Admin authorization resource for managing Sets, Artwork, and minting
  pub resource Admin {

    // Create a brand new artwork. Does not associate it with a set. Returns artId
    pub fun createArt(mediaType: String?, dataType: String?, data1: String?, data2: String?,
                   royalties: {String: Royalty}, metadata: {String: String}): UInt32 {
      let newArt = Art(
        mediaType: mediaType,
        dataType: dataType,
        data1: data1,
        data2:data2,
        royalties: royalties,
        metadata: metadata
      )

      FreshArtNFT.artwork[newArt.artId] = newArt

      return newArt.artId
    }

    pub fun createSet(name: String, description: String?) {
      var newSet <- create Set(name: name, description: description)
      FreshArtNFT.sets[newSet.setId] <-! newSet
    }

    pub fun borrowSet(setId: UInt32): &Set {
      pre { FreshArtNFT.sets[setId] != nil: "Cannot borrow set: The set doesn't exist." }
      return &FreshArtNFT.sets[setId] as &Set
    }

    pub fun startNewSeries(): UInt32 {
      FreshArtNFT.currentSeries = FreshArtNFT.currentSeries + (1 as UInt32)
      emit NewSeriesStarted(newCurrentSeries: FreshArtNFT.currentSeries)
      return FreshArtNFT.currentSeries
    }

    pub fun createNewAdmin(): @Admin {
      return <-create Admin()
    }
  }

  // ----------------------------------------------------------------
  // Contract Functions

  // User's new empty collection
  pub fun createEmptyCollection(): @FreshArtNFT.Collection {
    return <- create Collection()
  }

  pub fun getArtwork(artId: UInt32): FreshArtNFT.Art? {
    return FreshArtNFT.artwork[artId]
  }

  pub fun getAllArtwork(): [FreshArtNFT.Art] {
    return FreshArtNFT.artwork.values
  }

  pub fun getSetName(setId: UInt32): String? {
    return FreshArtNFT.setDatas[setId]?.name
  }

  pub fun getSetDescription(setId: UInt32): String? {
    return FreshArtNFT.setDatas[setId]?.description
  }

  pub fun getSetSeries(setId: UInt32): UInt32? {
    return FreshArtNFT.setDatas[setId]?.series
  }

  // Get the Ids that the specified Set name is associated with
  pub fun getSetIdsByName(setName: String): [UInt32]? {
    var setIds: [UInt32] = []
    for setData in FreshArtNFT.setDatas.values {
      if setName == setData.name {
        setIds.append(setData.setId)
      }
    }
    if setIds.length > 0 {
        return setIds
    }
    return nil
  }

  pub fun getArtIdsInSet(setId: UInt32): [UInt32]? {
    return FreshArtNFT.sets[setId]?.artIds
  }

  // Indicates if a Set/Art combo (otherwise known as an edition) is retired
  pub fun isEditionRetired(setId: UInt32, artId: UInt32): Bool? {
    let setRef = &FreshArtNFT.sets[setId] as &FreshArtNFT.Set
    return setRef.retired[artId]
  }

  pub fun isSetLocked(setId: UInt32): Bool? {
    return FreshArtNFT.sets[setId]?.locked
  }

  // Total number of Collectibles that have been minted from an edition
  pub fun getNumberCollectiblesInEdition(setId: UInt32, itemId: UInt32): UInt32? {
    let setRef = &FreshArtNFT.sets[setId] as &FreshArtNFT.Set
    return setRef.numberMintedPerItem[itemId]
  }


  // ----------------------------------------------------------------
  init() {
    // Set our named paths
    self.CollectionStoragePath = /storage/FreshArtCollection
    self.CollectionPublicPath = /public/FreshArtCollection
    self.AdminStoragePath = /storage/FreshArtAdmin

    // Initialize variables stored in the Contract
    self.currentSeries = 0
    self.artwork = {}
    self.setDatas = {}
    self.sets <- {}
    self.nextArtId = 1
    self.nextSetId = 1
    self.totalSupply = 0

    // Create a Collection resource and save it to storage
    self.account.save(<- create Collection(), to: self.CollectionStoragePath)

    // Create a public capability for the collection
    self.account.link<&{FreshArtNFT.CollectionPublic}>(
      self.CollectionPublicPath,
      target: self.CollectionStoragePath
    )

    // Create a Admin resource and save it to storage
    self.account.save(<- create Admin(), to: self.AdminStoragePath)

    emit ContractInitialized()
  }
}